#! /usr/bin/env python

# All textures within Content/source/Textures should be '@2x' textures, drawn at
# twice the DPI of a conventional monitor and at the DPI of the iPad 3 or Retina
# MacBook Pro. These textures will be downsampled via ImageMagick and saved with
# an '-sd' extension.
#
# -eropple
# 7 Mar 2013

import os
import sys
import shutil
import time
import subprocess
import string
import platform
from datetime import datetime
from subprocess import call

def process_texture_downsamples(CONTENT_ROOT, ASSET_ROOT):
    print "  > Processing textures for downsampled -sd versions..."

    TEX_DIR = CONTENT_ROOT + "/source/Textures"
    TEX_OUT = ASSET_ROOT + "/Textures"
    os.makedirs(TEX_OUT)

    for (rootdir, subdirs, files) in os.walk(TEX_DIR):
        key_root = rootdir.replace(TEX_DIR, "")[1:]

        tex_save_path = TEX_OUT + "/" + key_root
        if not os.path.exists(tex_save_path):
            call(["mkdir", "-p", tex_save_path])
        print "tsp: " + tex_save_path
        for file in files:
            if file.endswith(".png"):
                source_path = rootdir + "/" + file
                print "      - " + source_path
                copy_path = tex_save_path + "/" + file
                resize_path = tex_save_path + "/" + file.replace(".png", "-sd.png")

                shutil.copy(source_path, copy_path)
                args = [convert, \
                        source_path, "-resize", "50%", resize_path]
#                print string.join(args, " ")
                retval = call(args, shell=True)
                if retval != 0:
                    print " ERROR, code " + str(retval) + "; command line: " + string.join(args, "' '")
                    sys.exit(retval)


if len(sys.argv) < 3:
    print "ERROR: invalid usage."
    print "USAGE: " + sys.argv[0] + " CONTENT_ROOT ASSET_ROOT"
    sys.exit(1)


process_texture_downsamples(sys.argv[1], sys.argv[2])
