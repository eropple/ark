#! /usr/bin/env python

# Uses Glyph Designer Command Line (GDCL) to construct a font and font image
# from a Glyph Designer project file in Content/source/fonts. The font will be
# created at the project's specified size and named with no special extension;
# the same project will also be created with -rfs 0.5 (50% size) and saved with
# the '-sd' extension.
#
# -eropple
# 7 Mar 2013

import os
import sys
import shutil
import time
import subprocess
import string
import platform
from datetime import datetime
from subprocess import call

def process_fonts(CONTENT_ROOT, ASSET_ROOT):
    print "  > Processing fonts (retina and -sd)..."

    FONT_DIR = CONTENT_ROOT + "/source/Fonts"
    FONT_OUT = ASSET_ROOT + "/Fonts"

    if not os.path.exists(FONT_DIR):
        sys.exit(0)

    if not os.path.exists(FONT_OUT):
        os.makedirs(FONT_OUT)

    gdcl_path = "/Applications/GlyphDesigner.app/Contents/MacOS/GDCL"

    for file in os.listdir(FONT_DIR):
        sys.stdout.write("   - " + file + " ...")

        script_name = file.replace(".GlyphProject", "")

        source_path = FONT_DIR + "/" + file
        dest_path = FONT_OUT + "/" + script_name
        dest_sd_path = dest_path + "-sd"

        args = [gdcl_path, source_path, dest_path, \
                "-fo", "XML-fnt", "-rfs", "1.0"]
        args_sd = [gdcl_path, source_path, dest_sd_path, \
                "-fo", "XML-fnt", "-rfs", "0.5"]

#        print string.join(args, " ")
#        print string.join(args_sd, " ")

        retval1 = call(string.join(args, " "), shell=True)
        if retval1 != 0:
            print " ERROR code " + str(retval1) + "; command line: " + string.join(args, "' '")
            sys.exit(retval1)

        retval2 = call(string.join(args_sd, " "), shell=True)
        if retval2 != 0:
            print " ERROR code " + str(retval2) + "; command line: " + string.join(args_sd, "' '")
            sys.exit(retval2)


        print "done"


if len(sys.argv) < 3:
    print "ERROR: invalid usage."
    print "USAGE: " + sys.argv[0] + " CONTENT_ROOT ASSET_ROOT"
    sys.exit(1)


process_fonts(sys.argv[1], sys.argv[2])
