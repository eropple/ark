#! /usr/bin/env python

# Handles the content build step for Anaeax. Based on Anaeon's content
# system, itself based on Archos's content system...this thing has been
# around a while, this is rewrite four or something.
#
# -eropple
# 18 Jul 2012

import os
import sys
import shutil
import time
import subprocess
import string
import platform
from datetime import datetime
from subprocess import call

def main():
    print "Ark Content Build"
    print "================="
    print

    if len(sys.argv) < 4:
        print "Usage: %s CONTENT_ROOT ASSET_ROOT SCRIPT_ROOT1 [SCRIPT_ROOTn]" % os.path.basename(sys.argv[0])
        print "       CONTENT_ROOT is the parent directory of source and prebuilt."
        print "       ASSET_ROOT is where you want the compiled content to go."
        print "       SCRIPT_ROOTn is where you can store your own Python scripts"
        print "           for processing your data; the default will NOT be used"
        print "           if this option is chosen so you'll need to copy any"
        print "           default scripts into your own SCRIPT_ROOTs."
        sys.exit(1)

    CONTENT_ROOT = os.path.realpath(sys.argv[1])
    if not os.path.exists(CONTENT_ROOT) or not os.path.isdir(CONTENT_ROOT):
        print
        print
        print "ERROR: Content root '%s' does not exist or is not a directory." \
            % CONTENT_ROOT
        sys.exit(2)
    ASSET_ROOT = os.path.realpath(sys.argv[2])
    if os.path.exists(ASSET_ROOT) and not os.path.isdir(ASSET_ROOT):
        print
        print
        print "ERROR: Asset root '%s' already exists but is not a directory." \
                % ASSET_ROOT
        print
        sys.exit(2)

    SCRIPT_ROOTS = list()
    for n in range(3, len(sys.argv)):
        SCRIPT_ROOTS.append(sys.argv[n])

    print "Content root: " + CONTENT_ROOT
    print "Asset root:   " + ASSET_ROOT
    print "Script roots:"
    for root in SCRIPT_ROOTS:
        print " - " + root

    start_time = time.time()

    old_dir = ASSET_ROOT + "_old"

    if os.path.exists(ASSET_ROOT):
        shutil.rmtree(ASSET_ROOT)

    print "> Creating '%s' with prebuilt assets..." % ASSET_ROOT
    print
    prebuilt_dir = os.path.join(CONTENT_ROOT, "prebuilt")
    shutil.copytree(prebuilt_dir, ASSET_ROOT)

    failures = False

#   Not using standard texture atlases anymore.
#   process_texture_atlases(CONTENT_ROOT, ASSET_ROOT)

    for root in SCRIPT_ROOTS:
        print "> Processing scripts in '" + root + "'..."
        for script in os.listdir(root):
            if script.endswith(".py"):
                retval = call(["python", root + "/" + script, CONTENT_ROOT, ASSET_ROOT])
                if retval != 0:
                    failures = True
                    print ">! ABORTING: '" + script + "' returned code " + str(retval)



    end_time = time.time()

    print
    print "start time: %s" % time.strftime("%d %b %Y %I:%M:%S %p", \
            time.localtime(start_time))
    print "  end time: %s" % time.strftime("%d %b %Y %I:%M:%S %p", \
            time.localtime(end_time))
    print "total time: %s seconds" % str(end_time - start_time)

    if failures:
        print
        print "!!! WARNING !!!"
        print "Content build failed. Check the logs!"
        sys.exit(254)

"""
Uses TexturePackerPro to build texture atlases from directories within
Content/source/Atlases. Content/source/Atlases/TestAtlas becomes TestAtlas.atlas
and TestAtlas/a.png becomes "a" within TestAtlas.atlas.
"""
def process_texture_atlases(CONTENT_ROOT, ASSET_ROOT):
    SPRITE_SHEET_FORMAT = "xml"

    SYSTEM_TYPE = platform.uname()[0]

    if SYSTEM_TYPE == "Windows":
        TEXTURE_PACKER = "C:\\Program Files (x86)\\CodeAndWeb\\TexturePacker\\bin\\TexturePacker.exe"
    elif SYSTEM_TYPE == "Darwin" or SYSTEM_TYPE == "Linux":
        TEXTURE_PACKER = "/usr/local/bin/TexturePacker"
    else:
        print "Couldn't understand system type."
        sys.exit(100)


    ATLAS_ROOT = os.path.join(CONTENT_ROOT, "source", "Atlases")
    PNG_OUTPUT = os.path.join(ASSET_ROOT, "Atlases")
    ATL_OUTPUT = os.path.join(ASSET_ROOT, "Atlases")

    error = False

    if not os.path.exists(ATL_OUTPUT):
        os.makedirs(ATL_OUTPUT)
    if not os.path.exists(PNG_OUTPUT):
        os.makedirs(PNG_OUTPUT)

    for file in os.listdir(ATLAS_ROOT):
        os.chdir(ATLAS_ROOT)
        if os.path.isdir(file):
            full_dir = os.path.join(ATLAS_ROOT, file)

            sys.stdout.write(file + " ...")
            atl_img = os.path.join(PNG_OUTPUT, "%s.png" % file)
            atl_txt = os.path.join(ATL_OUTPUT, "%s.atlas" % file)

            if SYSTEM_TYPE == "Windows":
                cmd = [ TEXTURE_PACKER, "--quiet", \
                        "--algorithm", "MaxRects", \
                        "--trim-sprite-names", \
                        "--format", SPRITE_SHEET_FORMAT, \
                        "--disable-rotation", "--no-trim", \
                        "--sheet", atl_img, "--data", atl_txt ]

                # if the atlas name has -hd or @2x in it, we want
                # to automatically build a standard-definition version.
                if file.endswith("-hd") or file.endswith("@2x"):
                    cmd.insert(1, "--auto-sd")

                os.chdir(full_dir)
                for img in os.listdir(full_dir):
                    if img.endswith(".png"):
                        cmd.append(img)
            else:
                cmd = [ TEXTURE_PACKER + " --quiet --algorithm MaxRects " + \
                        "--trim-sprite-names --format " + SPRITE_SHEET_FORMAT + " " + \
                        "--disable-rotation --no-trim " + \
                        "--sheet " + atl_img + " --data " + atl_txt ]

                # if the atlas name has -hd or @2x in it, we want
                # to automatically build a standard-definition version.
                if file.endswith("-hd") or file.endswith("@2x"):
                    cmd[0] = cmd[0].replace("--quiet", "--quiet --auto-sd")

                os.chdir(full_dir)
                for img in os.listdir(full_dir):
                    if img.endswith(".png"):
                        cmd[0] = cmd[0] + " " + img


            # print cmd
            with open(os.devnull, 'w') as devnull:
                retval = subprocess.call(cmd, shell=True, \
                        stderr=devnull, stdout=devnull);

            r = retval
            if r == 0:
                if file.endswith("-hd") or file.endswith("@2x"):
                    print "done"
                else:
                    print "done (warning - no @2x version)"
                return 0
            else:
                print "error! code %d" % r
                return r


main()
