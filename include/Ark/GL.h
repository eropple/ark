//
//  GL.h
//  Ark
//
//  Created by Ed Ropple on 1/26/13.
//  Copyright (c) 2013 Large Russian Games. All rights reserved.
//

#pragma once

#include <GL/glew.h>
#if MACOS || IOS
    #include <OpenGL/OpenGL.h>
#else
    #include <GL/gl.h>
#endif