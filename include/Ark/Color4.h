//
//  Color4.h
//  Ark
//
//  Created by Ed Ropple on 1/23/13.
//  Copyright (c) 2013 Large Russian Games. All rights reserved.
//

#pragma once

#include <glm/glm.hpp>

namespace Ark
{
    class Color4
    {
        public:
            Color4(float red, float green, float blue, float alpha)
                : r(red), g(green), b(blue), a(alpha) {}
            Color4(unsigned char red, unsigned char green, unsigned char blue, unsigned char alpha);
            Color4() : r(0), g(0), b(0), a(1) {}
            
            float r;
            float g;
            float b;
            float a;
        
            inline unsigned char r_byte() const { return (unsigned char)(r * 255); }
            inline unsigned char g_byte() const { return (unsigned char)(g * 255); }
            inline unsigned char b_byte() const { return (unsigned char)(b * 255); }
            inline unsigned char a_byte() const { return (unsigned char)(a * 255); }
        
            inline glm::vec4 as_vec4() const { return glm::vec4(r, g, b, a); }
    };
}