//
//  TextDrawables.h
//  Ark
//
//  Created by Ed Ropple on 3/15/13.
//  Copyright (c) 2013 Large Russian Games. All rights reserved.
//

#pragma once

#include <Ark/Drawable.h>
#include <Ark/Color4.h>

#include <boost/noncopyable.hpp>

#include <vector>

namespace Ark
{
    class LineDrawable;
    class GlyphDrawable;

    class FontDescription;
    class FontCharacter;
    class SimpleTexturedQuad;

    class TextDrawable : public Drawable, private boost::noncopyable
    {
        public:
            TextDrawable(const std::vector<LineDrawable*> lines);
            ~TextDrawable();
        
            virtual void draw(const glm::mat4& modelview, const glm::mat4& projection) const;
        private:
            std::vector<LineDrawable*> _lines;
    };
    
    class LineDrawable : public Drawable, private boost::noncopyable
    {
        public:
            LineDrawable(const glm::vec3 offset, const FontDescription* font, const std::vector<GlyphDrawable*> glyphs);
            ~LineDrawable();
        
            inline const FontDescription* font() const { return _font; }
        
            virtual void draw(const glm::mat4& modelview, const glm::mat4& projection) const;
        private:
            glm::vec3 _offset;
        
            const FontDescription* _font;
            std::vector<GlyphDrawable*> _glyphs;
    };
    
    class GlyphDrawable : public Drawable, private boost::noncopyable
    {
        public:
            GlyphDrawable(const FontCharacter* character, const Color4& color, SimpleTexturedQuad* glyph_quad);
            ~GlyphDrawable();
        
            inline const FontCharacter* character() const { return _character; }
        
            virtual void draw(const glm::mat4& modelview, const glm::mat4& projection) const;
        private:
            Color4 _color;
        
            const FontCharacter* _character;
            SimpleTexturedQuad* _glyph_quad;
    };
}