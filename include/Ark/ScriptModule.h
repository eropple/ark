//
//  ScriptModule.h
//  Ark
//
//  Created by Ed Ropple on 1/17/13.
//  Copyright (c) 2013 Large Russian Games. All rights reserved.
//

#pragma once

#include <angelscript.h>

#include <boost/filesystem.hpp>
#include <boost/unordered_map.hpp>

#include <physfs.h>

namespace Ark
{
    namespace fs = boost::filesystem;

    class ScriptModule : private boost::noncopyable
    {
        public:
            ScriptModule(const boost::unordered_map<std::string, PHYSFS_File*>& files);
            ~ScriptModule();
        
            const std::string& name() const { return _name; }
        
            const asIScriptModule* module() const { return _module; }
        private:
            std::string _name;
        
            asIScriptModule* _module;
    };
}