//
//  GLFW.h
//  Ark
//
//  Created by Ed Ropple on 2/12/13.
//  Copyright (c) 2013 Large Russian Games. All rights reserved.
//

#pragma once

#ifdef DESKTOP

#include <boost/noncopyable.hpp>
#include <GL/glfw3.h>
#include <glm/glm.hpp>

namespace Ark
{
    class InitArgs;

    class GLFW : private boost::noncopyable
    {
        public:
            static void initialize(const InitArgs& args);
            static void teardown();
            static GLFW* instance();
        
            inline const glm::ivec2& window_bounds() const { return _window_bounds; }
            inline const glm::ivec2& virtual_bounds() const { return _virtual_bounds; }
        
            void ortho_projection(glm::mat4& mtx) const;
            void set_default_viewport();
        
            void swap_buffers() const;
        
            float scale_factor() const;
        protected:
        private:
            GLFW(const InitArgs& args);
            ~GLFW();

            GLFWwindow* _window;
            
            glm::ivec2 _window_bounds;
            glm::ivec2 _virtual_bounds;



            static void glfw_error_callback(int error, const char* description);
            static void glfw_resize_callback(GLFWwindow* window, int width, int height);
    };
    
    inline GLFW* glfw() { return GLFW::instance(); }
}

#endif