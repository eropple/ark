//
//  InitArgs.h
//  Ark
//
//  Created by Ed Ropple on 1/24/13.
//  Copyright (c) 2013 Large Russian Games. All rights reserved.
//

#pragma once

#include <Ark/ContentHeap.h>
#include <Ark/Exception.h>

#include <string>

namespace Ark
{
    namespace detail {
#ifdef WIN32
        typedef logog::FormatterMSVC FormatterBase;
#else
        typedef logog::FormatterGCC FormatterBase;
#endif
        
        class ArkFormatter : public FormatterBase
        {
        public:
            
            virtual TOPIC_FLAGS GetTopicFlags( const logog::Topic &topic )
            {
                TOPIC_FLAGS ret = FormatterBase::GetTopicFlags(topic);
                if (hideCPPDetails) {
                    ret = ret & ~(TOPIC_FILE_NAME_FLAG | TOPIC_LINE_NUMBER_FLAG);
                }
                return ret;
            }
        
            ArkFormatter() : FormatterBase() {
                hideCPPDetails = false;
            }
            
            void SetHideCPPDetails(bool hidden) {
                hideCPPDetails = hidden;
            }
            
            bool hideCPPDetails;
        };
    }
    
    class InitArgs
    {
        public:
            InitArgs(const std::vector<std::string>& args,
                     std::string company_name,
                     std::string application_name,
                     ContentHeap* global_content_heap,
                     bool allow_custom_file_search_paths = false,
                     bool resizable = false,
                     bool scale_to_hd = false)
                :   _args(args),
                    _company_name(company_name), _application_name(application_name),
                    _global_content_heap(global_content_heap),
                    _allow_custom_file_search_paths(allow_custom_file_search_paths),
                    _resizable(resizable),
                    _scale_to_hd(scale_to_hd) {}
            ~InitArgs()
            {
                if (_company_name.empty()) throw InitException("InitArgs.company_name must not be empty.");
                if (_application_name.empty()) throw InitException("InitArgs.company_name must not be empty.");
                if (_global_content_heap == nullptr) throw InitException("InitArgs.global_content_heap must not be nullptr.");
            }
        
            inline const std::vector<std::string>& args() const { return _args; }
        
            inline const std::string& company_name() const { return _company_name; }
            inline const std::string& application_name() const { return _application_name; }
        
            inline ContentHeap* global_content_heap() const { return _global_content_heap; }
        
            inline bool resizable() const { return _resizable; }
            inline bool allow_custom_file_search_paths() const { return _allow_custom_file_search_paths; }

        private:
            std::vector<std::string> _args;

            std::string _company_name;
            std::string _application_name;
        
            ContentHeap* _global_content_heap;
        
            bool _resizable;
            bool _allow_custom_file_search_paths;
            bool _scale_to_hd;
    };
    
    void initialize(const InitArgs& args);
    void teardown();
}