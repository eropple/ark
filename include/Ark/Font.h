//
//  Font.h
//  Ark
//
//  Created by Ed Ropple on 2/18/13.
//  Copyright (c) 2013 Large Russian Games. All rights reserved.
//

#pragma once

#include <boost/noncopyable.hpp>
#include <boost/unordered_map.hpp>

#include <Ark/SimpleTexturedQuad.h>

namespace Ark
{
    class FontDescription;
    class GLTexture;
    
    class TextDrawable;

    class Font : private boost::noncopyable
    {
        // This class is designed to provide resolution-independent font rendering controls.
        // It requires the content pipeline to produce two BMFonts at font sizes X and X/2.
        // On HiDPI displays, both of them will be loaded and the standard (X/2) font will
        // be used for font geometry (line-height, baseline, xadvance, etc.) but the HiDPI
        // font will be used for providing the actual rendered textures. On standard displays,
        // the standard font will be used for geometry and textures.
    
        public:
            Font(const std::string& font_name, bool hidpi);
            ~Font();
        
            inline const FontDescription* standard_font() const { return _standard_font; }
            inline const FontDescription* hidpi_font() const { return _hidpi_font; }
        
            inline const boost::unordered_map<int, GLTexture*>& textures() const { return _textures; }
            inline const boost::unordered_map<int, SimpleTexturedQuad*>& quads() const { return _quads; }
        
            const GLTexture* texture(int character) const;
            SimpleTexturedQuad* quad(int character) const;
        
            float measure_string_height() const;
            float measure_string_width(const std::string& text, bool strip_codes = true) const;
        
            TextDrawable* build_text_drawable(const std::string& text,
                                              const Color4& color = Color4(1.0f, 1.0f, 1.0f, 1.0f),
                                              int wrap_length = -1);
        
            static std::string strip_color_codes(const std::string& text);
        private:
            FontDescription* _standard_font;
            FontDescription* _hidpi_font;
        
            boost::unordered_map<int, GLTexture*> _textures;
            boost::unordered_map<int, SimpleTexturedQuad*> _quads;
    };
}