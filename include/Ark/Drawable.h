//
//  Drawable.h
//  Ark
//
//  Created by Ed Ropple on 12/28/12.
//  Copyright (c) 2012 Large Russian Games. All rights reserved.
//

#pragma once

#include <glm/glm.hpp>

namespace Ark
{
    class Drawable
    {
        public:
            virtual void draw(const glm::mat4& modelview, const glm::mat4& projection) const = 0;
        protected:
        private:
    };
}