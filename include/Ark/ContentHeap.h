//
//  ContentHeap.h
//  Ark
//
//  Created by Ed Ropple on 12/26/12.
//  Copyright (c) 2012 Large Russian Games. All rights reserved.
//

#pragma once

#include <Ark/GLTexture.h>
#include <Ark/ScriptModule.h>
#include <Ark/ShaderProgram.h>
#include <Ark/MemoryBitmap.h>

#include <boost/filesystem.hpp>
#include <boost/unordered_map.hpp>

#include <string>

#include <physfs.h>

namespace Ark
{
    namespace fs = boost::filesystem;
    
    class GLTexture;
    class InitArgs;
    
    class ContentHeap : private boost::noncopyable
    {
        friend void initialize(const InitArgs& args);
    
        public:
            ContentHeap(ContentHeap* parent);
            virtual ~ContentHeap();
        
            inline ContentHeap* parent() const { return _parent; }
            inline unsigned int heap_id() const { return _heap_number; }
            inline float scale_factor() const { return _scale_factor; }
        
            virtual ContentHeap* create_child_heap();
        
            GLTexture* load_texture(const std::string& key);
            GLTexture* get_texture_if_loaded(const std::string& key) const;
            GLTexture* load_texture_unmanaged(const std::string& key) const;
        
            ScriptModule* load_script_module(const std::string& key);
            ScriptModule* get_script_module_if_loaded(const std::string& key) const;
            ScriptModule* load_script_module_unmanaged(const std::string& key) const;
        
            MemoryBitmap* load_memory_bitmap(const std::string& key);
            MemoryBitmap* get_memory_bitmap_if_loaded(const std::string& key) const;
            MemoryBitmap* load_memory_bitmap_unmanaged(const std::string& key) const;
        
        protected:
            void add_child_heap(ContentHeap* heap) { child_heaps.push_back(heap); }
        
        private:
            float _scale_factor;
        
            std::vector<ContentHeap*> child_heaps;
        
            unsigned int _heap_number;
        
            ContentHeap* _parent;
        
            void remove_child(ContentHeap* child_heap);
        
            boost::unordered_map<std::string, GLTexture*> textures;
            boost::unordered_map<std::string, ScriptModule*> script_modules;
            boost::unordered_map<std::string, MemoryBitmap*> memory_bitmaps;
    };
}