//
//  Polygon.h
//  Ark
//
//  Created by Ed Ropple on 1/16/13.
//  Copyright (c) 2013 Large Russian Games. All rights reserved.
//

#pragma once

#include <cmath>
#include <vector>

#include <glm/glm.hpp>

namespace Ark
{
    class Triangle
    {
        public:
            Triangle(glm::vec2 a_, glm::vec2 b_, glm::vec2 c_)
                : a(a_), b(b_), c(c_)
            {
                
            }
            Triangle(const Triangle& source)
                : a(source.a), b(source.b), c(source.c)
            {
            }
        
            ~Triangle() {}
        
            glm::vec2 a;
            glm::vec2 b;
            glm::vec2 c;
        
            float length_ab() const
            {
                return sqrtf( powf(b.x - a.x, 2) - powf(b.y - a.y, 2) );
            }
            float length_bc() const
            {
                return sqrtf( powf(c.x - b.x, 2) - powf(c.y - b.y, 2) );
            }
            float length_ac() const
            {
                return sqrtf( powf(c.x - a.x, 2) - powf(c.y - a.y, 2) );
            }
        
            float angle_a() const
            {
                float ab = length_ab();
                float bc = length_bc();
                float ac = length_ac();
            
                return acosf( (powf(ab, 2) + powf(ac, 2) - powf(bc, 2) ) / (2 * ab * ac) );
            }
            float angle_b() const
            {
                float ab = length_ab();
                float bc = length_bc();
                float ac = length_ac();
                
                return acosf( (powf(ab, 2) + powf(bc, 2) - powf(ac, 2) ) / (2 * ab * bc) );
            }
            float angle_c() const
            {
                float ab = length_ab();
                float bc = length_bc();
                float ac = length_ac();
                
                return acosf( (powf(ac, 2) + powf(bc, 2) - powf(ab, 2) ) / (2 * ac * bc) );
            }
    };

    class Polygon
    {
        public:
            Polygon(const std::vector<glm::vec2>& input_vertices)
                : _vertices(input_vertices)
            {
                assert( input_vertices.size() > 0 );
            }
            Polygon(const Polygon& source)
                : _vertices(source._vertices)
            {
            }
        
            ~Polygon()
            {
                
            }
        
            const std::vector<glm::vec2>& vertices() const { return _vertices; }
        
            bool collide(const Polygon& other, glm::vec2& penetrate) const;
        
            std::vector<Triangle> build_triangle_fan() const
            {
                std::vector<Triangle> triangles;
                for (int i = 1; i < _vertices.size() - 1; ++i)
                {
                    triangles.push_back(Triangle(_vertices[0], _vertices[i], _vertices[i+1]));
                }
                
                return triangles;
            }
        
        
        private:
            std::vector<glm::vec2> _vertices;
    };
}