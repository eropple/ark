//
//  FontDescription.h
//  Ark
//
//  Created by Ed Ropple on 2/16/13.
//  Copyright (c) 2013 Large Russian Games. All rights reserved.
//

#pragma once

#include <Ark/GLTexture.h>
#include <Ark/Rect.h>

#include <boost/unordered_map.hpp>

#include <glm/glm.hpp>

namespace Ark
{
    class FontCharacter : private boost::noncopyable
    {
        public:
            FontCharacter(int letter, int x, int y, int width, int height,
                          int xoffset, int yoffset, int xadvance,
                          int page, int channel)
                :   _letter(letter), _rect(x, y, width, height),
                    _offset(xoffset, yoffset), _xadvance(xadvance), _page(page), _channel(channel) {}
            ~FontCharacter() {}
        
            inline char letter() const { return _letter; }
            inline const IntRect& rect() const { return _rect; }
            inline const glm::ivec2& offset() const { return _offset; }
            inline int xadvance() const { return _xadvance; }
            inline int page() const { return _page; }
            inline int channel() const { return _channel; }
        
        private:
            char _letter;
            IntRect _rect;
            glm::ivec2 _offset;
            int _xadvance;
            int _page;
            int _channel;
    };

    class FontDescription : private boost::noncopyable
    {
        public:
            FontDescription(const std::string& key);
            ~FontDescription();
        
            inline int size() const { return _size; }
            inline int line_height() const { return _line_height; }
            inline int baseline() const { return _baseline; }
        
            inline const boost::unordered_map<int, std::string>& pages() const { return _pages; }
            inline const boost::unordered_map<int, FontCharacter*>& characters() const { return _characters; }
            inline const boost::unordered_map<std::pair<int, int>, float>& kernings() const { return _kernings; }
        
            const FontCharacter* character(int char_value) const;
            float kerning(int char1, int char2) const;
        private:
            int _size;
            int _line_height;
            int _baseline;
        
            boost::unordered_map<int, std::string> _pages;
            boost::unordered_map<int, FontCharacter*> _characters;
            boost::unordered_map<std::pair<int, int>, float> _kernings;
    };
}