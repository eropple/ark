//
//  Rect.h
//  Ark
//
//  Created by Ed Ropple on 1/23/13.
//  Copyright (c) 2013 Large Russian Games. All rights reserved.
//

#pragma once

#include <vector>

#include <glm/glm.hpp>

namespace Ark
{
    class FloatRect
    {
        public:
            FloatRect(const glm::vec2& top_left, const glm::vec2& bottom_right)
                : x(top_left.x), y(top_left.y),
                  width(bottom_right.x - top_left.x), height(bottom_right.y - top_left.y) {}
            FloatRect(float x, float y, float width, float height)
                : x(x), y(y), width(width), height(height) {}
            FloatRect() : x(0), y(0), width(0), height(0) {}
        
            std::vector<glm::vec2> vertices() const;
        
            float x;
            float y;
            float width;
            float height;
        
            inline bool operator==(const FloatRect& other)
            {
                return x == other.x && y == other.y && width == other.width && height == other.height;
            }
            inline bool operator==(const FloatRect* other)
            {
                return x == other->x && y == other->y && width == other->width && height == other->height;
            }
        
            inline bool operator!=(const FloatRect& other) { return !(this == &other); }
            inline bool operator!=(const FloatRect* other) { return !(this == other); }
        
            ~FloatRect() {}
        private:
            
    };
    
    class IntRect
    {
        public:
            IntRect(const glm::ivec2& top_left, const glm::ivec2& bottom_right)
                :   x(top_left.x), y(top_left.y),
                    width(bottom_right.x - top_left.x), height(bottom_right.y - top_left.y) {}
            IntRect(int x, int y, int width, int height)
                : x(x), y(y), width(width), height(height) {}
            IntRect() : x(0), y(0), width(0), height(0) {}
            
            std::vector<glm::ivec2> vertices() const;
            
            int x;
            int y;
            int width;
            int height;
            
            inline bool operator==(const IntRect& other)
            {
                return x == other.x && y == other.y && width == other.width && height == other.height;
            }
            inline bool operator==(const IntRect* other)
            {
                return x == other->x && y == other->y && width == other->width && height == other->height;
            }
            
            inline bool operator!=(const IntRect& other) { return !(this == &other); }
            inline bool operator!=(const IntRect* other) { return !(this == other); }
            
            ~IntRect() {}
        private:
        
    };
}
