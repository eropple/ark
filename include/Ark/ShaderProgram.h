//
//  ShaderProgram.h
//  Ark
//
//  Created by Ed Ropple on 1/26/13.
//  Copyright (c) 2013 Large Russian Games. All rights reserved.
//

#pragma once

#include <Ark/GL.h>
#include <Ark/ContentHeap.h>
#include <Ark/Exception.h>

#include <boost/filesystem.hpp>
#include <boost/noncopyable.hpp>
#include <boost/unordered_set.hpp>
#include <boost/unordered_map.hpp>

#include <glm/glm.hpp>

#include <physfs.h>

namespace Ark
{
    class ShaderProgram : private boost::noncopyable
    {
        public:
            ShaderProgram(const std::vector<PHYSFS_File*>& vertex_shaders,
                          const std::vector<PHYSFS_File*>& fragment_shaders);
            ~ShaderProgram();
        
            GLuint program_id() const { return _program_id; }
        
            bool uniform_location(const std::string& name, GLint& location) const;
            bool attrib_location(const std::string& name, GLint& location) const;
        
            boost::unordered_set<std::string> uniform_names() const;
            boost::unordered_set<std::string> attrib_names() const;
        
            void use_program() const;
        
            void enable_attrib_arrays() const;
            void disable_attrib_arrays() const;
        
            bool vertex_attrib_pointer(const std::string& name,
                                       GLint size, GLenum type, GLboolean normalized,
                                       GLsizei stride, int offset) const;
        
            bool set_uniform(const std::string& name, const glm::vec2& v) const;
            bool set_uniform(const std::string& name, const glm::vec3& v) const;
            bool set_uniform(const std::string& name, const glm::vec4& v) const;
            bool set_uniform(const std::string& name, const glm::mat3& m) const;
            bool set_uniform(const std::string& name, const glm::mat4& m) const;
            bool set_uniform(const std::string& name, float val ) const;
            bool set_uniform(const std::string& name, int val ) const;
            bool set_uniform(const std::string& name, bool val ) const;
        
            static void release_program() { glUseProgram(NULL); }
        
            static ShaderProgram* from_yaml(const std::string& key);
        private:
            GLuint _program_id;
        
            boost::unordered_map<std::string, GLint> _uniform_locations;
            boost::unordered_map<std::string, GLint> _attribute_locations;
    };
}