//
//  file_utils.h
//  Ark
//
//  Created by Ed Ropple on 1/26/13.
//  Copyright (c) 2013 Large Russian Games. All rights reserved.
//

#pragma once

#include <string>

#include <boost/filesystem.hpp>
#include <yaml-cpp/yaml.h>
#include <rapidxml/rapidxml.hpp>

#include <physfs.h>

namespace Ark
{
    bool open_file(const std::string& key, PHYSFS_File** file);
    bool read_string_from_file(PHYSFS_File* file, std::string& str);
    bool read_vector_from_file(PHYSFS_File* file, std::vector<char>& vec);
    bool read_yaml_from_file(PHYSFS_File* file, YAML::Node& yaml);
    bool read_xml_from_file(PHYSFS_File* file, rapidxml::xml_document<>& xml);
    
    bool list_entities(const std::string& directory_path, std::vector<std::string>& entities);
}