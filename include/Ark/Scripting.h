//
//  Scripting.h
//  Ark
//
//  Created by Ed Ropple on 1/17/13.
//  Copyright (c) 2013 Large Russian Games. All rights reserved.
//

#pragma once

#include <boost/noncopyable.hpp>
#include <angelscript.h>

namespace Ark
{
    class Scripting : private boost::noncopyable
    {
        public:
            asIScriptEngine* engine() { return _engine; }
        
        
            static void initialize();
            static void teardown();
            static Scripting* instance();
            
        protected:
        private:
            Scripting();
            ~Scripting();
        
        
            asIScriptEngine* _engine;      
    };
    
    inline Scripting* scripting() { return Scripting::instance(); }
}
