//
//  GLTexture.h
//  Anaeax
//
//  Created by Ed Ropple on 1/24/13.
//  Copyright (c) 2013 Large Russian Games. All rights reserved.
//

#pragma once

#include <Ark/GL.h>
#include <Ark/ContentHeap.h>
#include <Ark/Rect.h>
#include <Ark/MemoryBitmap.h>

#include <boost/filesystem.hpp>
#include <boost/noncopyable.hpp>

#include <physfs.h>

namespace Ark
{
    class ContentHeap;

    class GLTexture : private boost::noncopyable
    {
        public:
            GLTexture(const std::string& key);
            GLTexture(const MemoryBitmap* bitmap);
            ~GLTexture() {}
        
            inline bool is_reloadable() const { return _reloadable; }
        
            inline GLuint texture_id() const { return _texture_id; }
            inline const FloatRect& bounds() const { return _bounds; }
        
            void reload(bool dispose_first = true);
        
            void bind(GLuint texture_unit = GL_TEXTURE0) const;
            void bind_inactive(GLuint texture_unit) const;
        
        private:
            ContentHeap* _heap;
            std::string _reload_key;
            bool _reloadable;
        
            GLuint _texture_id;
            FloatRect _bounds;
        
            void delete_texture();
            void load_texture(const MemoryBitmap* bitmap);
    };
}