//
//  StateManager.h
//  Ark
//
//  Created by Ed Ropple on 12/26/12.
//  Copyright (c) 2012 Large Russian Games. All rights reserved.
//
#pragma once

#include <Ark/ContentHeap.h>
#include <Ark/State.h>

namespace Ark
{
    class StateManager : private boost::noncopyable
    {
        public:
            void start_frame();
            void update(double delta);
            void draw(double delta, const glm::mat4& projection);
            void finish_frame();
        
            void push(State* newState);
            void pop();
            void pop_and_push(State* newState);
        
            void key_handler(int key_identifier, bool pressed);
            void char_handler(int unichar);
            void mouse_position_handler(int x, int y, int dX, int dY);
            void mouse_button_handler(int button, int x, int y, bool pressed);
            void mouse_wheel_handler(double wheel_position, int wheel_direction, int x, int y);
        
            bool has_exited();
            
            static void initialize(ContentHeap* global_heap);
            static void teardown();
            static StateManager* instance();
            
        protected:
            
        private:
            StateManager(ContentHeap* global_heap);
            ~StateManager();
        
            std::vector<State*> _states;
            std::vector<State*> _deadStates;
            
            bool _stack_is_dirty;
            
            ContentHeap* _global_heap;
    };
    
    inline StateManager* state_manager() { return StateManager::instance(); }
}