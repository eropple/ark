//
//  State.h
//  Ark
//
//  Created by Ed Ropple on 12/26/12.
//  Copyright (c) 2012 Large Russian Games. All rights reserved.
//

#pragma once

#include <Ark/ContentHeap.h>
#include <Ark/Exception.h>

#include <typeinfo>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

namespace Ark
{
    class State : private boost::noncopyable
    {
        public:
            virtual void do_initialize(ContentHeap* global_heap) = 0;
            virtual void do_update(double delta, bool top_of_stack) = 0;
            virtual void do_draw(double delta, const glm::mat4& projection, bool top_of_stack) = 0;
            
            virtual bool key_handler(int key_identifier, bool pressed) = 0;
            virtual bool char_handler(int unichar) = 0;
            virtual bool mouse_position_handler(int x, int y, int dX, int dY) = 0;
            virtual bool mouse_button_handler(int button, int x, int y, bool pressed) = 0;
            virtual bool mouse_wheel_handler(double wheel_position, int wheel_direction, int x, int y) = 0;
            
            virtual bool is_initialized() = 0;
            
            virtual bool soaks_updates() = 0;
            virtual bool soaks_draws() = 0;
            virtual bool soaks_input() = 0;
            
            virtual void lost_focus(State* newTopState) = 0;
            virtual void got_focus(State* oldTopState) = 0;
            
            virtual ~State() {}
    };
    
    template <typename TContentHeap>
    class BaseState : public State
    {
        public:
            virtual void do_initialize(ContentHeap* global_heap)
            {
                if (initialized) return; // it is entirely cromulent to try to initialize twice, off-thread loading will cause it.
                
                TContentHeap* global_core = dynamic_cast<TContentHeap*>(global_heap);
                    
                if (global_core == nullptr)
                {
                    throw StateException("State expected a heap of type " + std::string(typeid(TContentHeap).name()) +
                                         "and got one of type " + std::string(typeid(*global_heap).name()));
                }
            
                _global_heap = global_core;
                
                _local_heap = _global_heap->create_child_heap();
                
                this->initialize();
                initialized = true;
            }
        
            virtual void do_teardown()
            {
                if (toredown) return;
                
                this->teardown();
                toredown = true;
            }
        
            virtual void do_update(double delta, bool top_of_stack)
            {
                this->update(delta, top_of_stack);
            }
            virtual void do_draw(double delta, const glm::mat4& projection, bool top_of_stack)
            {
                this->draw(delta, projection, top_of_stack);
            }
        
            virtual bool key_handler(int key_identifier, bool pressed) { return false; };
            virtual bool char_handler(int unichar) { return false; };
            virtual bool mouse_position_handler(int x, int y, int dX, int dY) { return false; };
            virtual bool mouse_button_handler(int button, int x, int y, bool pressed) { return false; };
            virtual bool mouse_wheel_handler(double wheel_position, int wheel_direction, int x, int y) { return false; };
        
            virtual bool is_initialized() { return initialized; }
            
            virtual bool soaks_updates() { return false; }
            virtual bool soaks_draws() { return false; }
            virtual bool soaks_input() { return true; }
            
            virtual void lost_focus(State* newTopState) {}
            virtual void got_focus(State* oldTopState) {}
            
            virtual ~BaseState()
            {
                if (_local_heap != nullptr) delete _local_heap;
            }
        protected:
            BaseState()
            {
                initialized = false;
                _global_heap = _local_heap = nullptr;
            }
            
            virtual void initialize() = 0;
            virtual void update(double delta, bool top_of_stack) = 0;
            virtual void draw(double delta, const glm::mat4& projection, bool top_of_stack) = 0;
            virtual void teardown() = 0;
            
            TContentHeap* global_heap() { return _global_heap; }
            TContentHeap* local_heap() { return _local_heap; }
        private:
            bool initialized;
            bool toredown;
            
            TContentHeap* _global_heap;
            TContentHeap* _local_heap;
    };
}