//
//  SimpleTexturedQuad.h
//  Ark
//
//  Created by Ed Ropple on 2/18/13.
//  Copyright (c) 2013 Large Russian Games. All rights reserved.
//

#include <Ark/RenderObject.h>

namespace Ark
{
    class GLTexture;
    class ShaderProgram;

    struct SimpleTexturedVertexData
    {
        glm::vec3 position;
        glm::vec2 texcoords;
    };
    
    class SimpleTexturedQuad : public RenderObject<SimpleTexturedVertexData>, private boost::noncopyable
    {
        public:
            SimpleTexturedQuad(glm::ivec2 size, GLTexture* texture, Color4 color = Color4(1.0f, 1.0f, 1.0f, 1.0f));
            virtual ~SimpleTexturedQuad();
        
            virtual void draw(const glm::mat4& modelview, const glm::mat4& projection) const;
        
            inline void set_color(const Color4& color)
            {
                _color = color;
                set_uniform("color", _color.as_vec4());
            }
            inline const Color4& color() const { return _color; }
        protected:
            static ShaderProgram* default_shader();
        private:
            Color4 _color;
            static RenderDefinition<SimpleTexturedVertexData>* build_definition(glm::ivec2 size);
    };
}