//
//  MemoryBitmap.h
//  Ark
//
//  Created by Ed Ropple on 1/24/13.
//  Copyright (c) 2013 Large Russian Games. All rights reserved.
//

#pragma once

#include <Ark/Color4.h>
#include <Ark/LodePNG.h>

#include <boost/filesystem.hpp>
#include <boost/noncopyable.hpp>
#include <vector>

#include <physfs.h>

namespace Ark
{
    // Class is used for loading and manipulating images in local memory. Image can consume
    // it to push things to the GPU.
    class MemoryBitmap : private boost::noncopyable
    {
        public:
            MemoryBitmap(PHYSFS_File* file);
            MemoryBitmap(unsigned int width, unsigned int height, const Color4& color);
            ~MemoryBitmap() {}
        
            // creates a _new_ memory bitmap using this slice. unmanaged afterwards!
            MemoryBitmap* copy_slice(unsigned int x, unsigned int y, unsigned int width, unsigned int height);
        
            inline unsigned int width() const { return _width; }
            inline unsigned int height() const { return _height; }
        
            inline const std::vector<unsigned char>& data() const { return _data; }
        
            inline Color4 get_color(unsigned int x, unsigned int y) const
            {
                // TODO: can probably be improved
                size_t offset = y * _width * + x * 4;
                return Color4(_data[offset], _data[offset + 1], _data[offset + 2], _data[offset + 3]);
            }
            inline unsigned int get(unsigned int x, unsigned int y) const
            {
                // TODO: can probably be improved
                size_t offset = y * _width * 4 + x * 4;
                return (_data[offset] << 24) | (_data[offset + 1] << 16) | (_data[offset + 2] << 8) | (_data[offset + 3]);
            }
        
            inline void set_color(unsigned int x, unsigned int y, const Color4& color)
            {
                size_t offset = y * _width * 4 + x * 4;
                
                // TODO: can probably be improved
                
                _data[offset] = color.r_byte();
                _data[offset + 1] = color.g_byte();
                _data[offset + 2] = color.b_byte();
                _data[offset + 3] = color.a_byte();
            }
        
            inline void set_color(unsigned int x, unsigned int y, int color)
            {
                size_t offset = y * _width * 4 + x * 4;
                
                // TODO: can probably be improved
                
                _data[offset] = color >> 24 & 0xFF;
                _data[offset + 1] = color >> 16 & 0xFF;
                _data[offset + 2] = color >> 8 & 0xFF;
                _data[offset + 3] = color & 0xFF;
            }
        
        private:
            unsigned int _width;
            unsigned int _height;
        
            std::vector<unsigned char> _data;
    };
}