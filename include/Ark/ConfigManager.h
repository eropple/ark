//
//  ConfigManager.h
//  Ark
//
//  Created by Ed Ropple on 12/26/12.
//  Copyright (c) 2012 Large Russian Games. All rights reserved.
//

#pragma once

#include <Ark/Platform.h>
#include <boost/program_options.hpp>

namespace Ark
{
    class ConfigManager : private boost::noncopyable
    {
        public:
            template<typename T>
            T get(const std::string& opt, const T& default_value) const
            {
                auto value = vm[opt];
                
                if (value.empty()) return default_value;
                
                return value.template as<T>();
            }
        
            const std::vector<std::string>& mod_names() const { return _mod_names; }
        
            static const ConfigManager* instance();
            static void initialize(const std::vector<std::string>& args,
                                   const std::vector<boost::filesystem::path>& config_search_paths);
            static void teardown();
        protected:
        private:
            ConfigManager(const std::vector<std::string>& args,
                          const std::vector<boost::filesystem::path>& config_search_paths);
            ~ConfigManager() {}
        
            boost::program_options::variables_map vm;
            std::vector<std::string> _mod_names;
            
            static bool conf_exists(const boost::filesystem::path& file)
            {
                return boost::filesystem::exists(file) && boost::filesystem::is_regular_file(file);
            }
    };
    
    inline const ConfigManager* config() { return ConfigManager::instance(); }
}