//
//  math_utils.h
//  Ark
//
//  Created by Ed Ropple on 1/20/13.
//  Copyright (c) 2013 Large Russian Games. All rights reserved.
//

#pragma once

#include <Ark/Rect.h>

namespace Ark
{
#ifdef _MSC_VER
    // part of C99, which MSVC doesn't support
    inline double roundf(double x) { return (x-floor(x))>0.5 ? ceil(x) : floor(x); }
#endif

    inline bool glm_vector_comparator (const glm::vec2& left, const glm::vec2& right)
    {
        return glm::dot(left, left) < glm::dot(right, right);
    }
    FloatRect compute_best_fit_rectangle(const FloatRect& rect, const FloatRect& container, bool round = true);
}