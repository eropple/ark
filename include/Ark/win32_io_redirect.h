//
//  win32_io_redirect.h
//  Ark
//
//  Created by Ed Ropple on 2/12/13.
//  Copyright (c) 2013 Large Russian Games. All rights reserved.
//

#pragma once

#if WIN32

namespace Ark
{
    namespace Win32
    {
        void redirect_io();
    }
}

#endif