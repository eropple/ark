//
//  Exception.h
//  Ark
//
//  Created by Ed Ropple on 1/26/13.
//  Copyright (c) 2013 Large Russian Games. All rights reserved.
//

#pragma once

#include <exception>
#include <logog/logog.hpp>

namespace Ark
{
    class Exception : public std::exception
    {
        public:
            Exception(const std::string& message)
                : _message(message)
            {
                LOGOG_ERROR("Exception here: %s", message.c_str());
            }
            virtual ~Exception() {}
        
            virtual const char* what() const throw() { return _message.c_str(); }
        
        private:
            std::string _message;
    };
    
    class InitException : public Exception
    {
        public:
            InitException(const std::string& message)
                : Exception(message) {}
            virtual ~InitException() {}
    };
    
    class StateException : public Exception
    {
        public:
            StateException(const std::string& message)
                : Exception(message) {}
            virtual ~StateException() {}
    };
    
    class ContentException : public Exception
    {
        public:
            ContentException(const std::string& message)
                : Exception(message) {}
            virtual ~ContentException() {}
    };
    
    class DrawableException : public Exception
    {
        public:
            DrawableException(const std::string& message)
                : Exception(message) {}
            virtual ~DrawableException() {}
    };
    
    class BadRenderObjectFormatException : public DrawableException
    {
        public:
            BadRenderObjectFormatException(const std::string& message)
            :   DrawableException(message) {}
            virtual ~BadRenderObjectFormatException() {}
    };
    
    class GLException : public Exception
    {
        public:
            GLException(const std::string& message)
                : Exception(message) {}
            virtual ~GLException() {}
    };
    
    class FontException : public Exception
    {
        public:
            FontException(const std::string& message)
                : Exception(message) {}
            virtual ~FontException() {}
    };
}
