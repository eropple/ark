//
//  platform.h
//  Ark
//
//  Created by Ed Ropple on 12/26/12.
//  Copyright (c) 2012 Large Russian Games. All rights reserved.
//

#pragma once

#include <boost/filesystem.hpp>

#include <string>

namespace Ark
{
    namespace Platform
    {
        boost::filesystem::path executable_path();
        boost::filesystem::path resource_root();
        boost::filesystem::path user_data_root();
    }}
