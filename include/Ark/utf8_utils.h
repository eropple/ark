//
//  utf8_utils.h
//  Ark
//
//  Created by Ed Ropple on 2/12/13.
//  Copyright (c) 2013 Large Russian Games. All rights reserved.
//

#include <utf8.h>

namespace Ark
{
    inline std::string utf16to8(const std::wstring& wstr)
    {
        std::string str;
        utf8::utf16to8(wstr.begin(), wstr.end(), std::back_inserter(str));
        return str;
    }

    inline std::string utf16to8(const wchar_t* wcstr)
    {
        return utf16to8(std::wstring(wcstr));
    }

    inline std::wstring utf8to16(const std::string& str)
    {
        std::wstring wstr;
        utf8::utf8to16(str.begin(), str.end(), std::back_inserter(wstr));
        return wstr;
    }

    inline std::wstring utf8to16(const char* cstr)
    {
        return utf8to16(std::string(cstr));
    }

    inline std::string utf8_filter(const std::string& str)
    {
        std::string fixed;
        utf8::replace_invalid(str.begin(), str.end(), std::back_inserter(fixed));
        return fixed;
    }
}