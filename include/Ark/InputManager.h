#pragma once
// InputManager.h - class definition for the event-based input system.

#include <boost/noncopyable.hpp>

class GLFWwindow;

namespace Ark
{
    class InputManager : private boost::noncopyable
    {
        public:
            InputManager(GLFWwindow* window);
            virtual ~InputManager();

            bool is_key_pressed(int key_identifier) const;

            static void initialize(GLFWwindow* window);
            static void teardown();
            static InputManager* instance();

        protected:
        private:
            GLFWwindow* _window;

            int last_x;
            int last_y;
            double last_wheel_position;

            void key_handler(int key_identifier, bool pressed);
            void char_handler(int unichar_identifier);
            void mouse_position_handler(int x, int y);
            void mouse_button_handler(int button, bool pressed);
            void mouse_wheel_handler(double wheel_position);

#if DESKTOP
            static void glfw_mouse_button_callback(GLFWwindow* window, int button, int action);
            static void glfw_cursor_position_callback(GLFWwindow* window, int x, int y);
            static void glfw_scroll_callback(GLFWwindow* window, double x, double y);
            static void glfw_key_callback(GLFWwindow* window, int key, int action);
            static void glfw_char_callback(GLFWwindow* window, int character);
#endif
    };
    
    inline InputManager* input() { return InputManager::instance(); }
}