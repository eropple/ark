//
//  RenderObject.h
//  Ark
//
//  Created by Ed Ropple on 1/26/13.
//  Copyright (c) 2013 Large Russian Games. All rights reserved.
//

#pragma once

#include <Ark/GL.h>
#include <Ark/GLTexture.h>
#include <Ark/Drawable.h>
#include <Ark/Exception.h>
#include <Ark/ShaderProgram.h>

#include <boost/noncopyable.hpp>
#include <boost/unordered_map.hpp>
#include <boost/algorithm/string.hpp>

#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/matrix_transform.hpp>

namespace Ark
{
    class RenderDefinitionItem
    {
        public:
            RenderDefinitionItem(const std::string& name, GLint size,
                                 GLenum type, GLboolean normalized, int offset)
                :   _name(name), _size(size), _type(type),
                    _normalized(normalized), _offset(offset) {}
            ~RenderDefinitionItem() {}
        
            inline const std::string& name() const { return _name; }
            inline GLint size() const { return _size; }
            inline GLenum type() const { return _type; }
            inline GLboolean normalized() const { return _normalized; }
            inline int offset() const { return _offset; }
        private:
            std::string _name;
            GLint _size;
            GLenum _type;
            GLboolean _normalized;
            int _offset;
    };
    
    template <typename TPODVertexData>
    class RenderDefinition : private boost::noncopyable
    {
        public:
            RenderDefinition(const std::vector<TPODVertexData>& vertices,
                             const std::vector<RenderDefinitionItem>& vertex_attribs,
                             const std::vector<unsigned short>& indices,
                             GLint draw_mode = GL_TRIANGLE_STRIP)
                :   _vertex_attribs(vertex_attribs), _vertex_count(vertices.size()),
                    _indices(indices), _draw_mode(draw_mode),
                    _stride(sizeof(TPODVertexData))
            {
                if (vertices.size() < 3) throw BadRenderObjectFormatException("RenderObject reqires at least 3 vertices.");
            
                GLuint id;
                GLuint* pid = &id;
                pid = pid;
                
                glGenBuffers(1, &id);
                glBindBuffer(GL_ARRAY_BUFFER, id);
                
                glBufferData(GL_ARRAY_BUFFER, sizeof(TPODVertexData) * vertices.size(), vertices.data(), GL_STATIC_DRAW);
                
                _buffer_id = id;
            }
            ~RenderDefinition()
            {
                glDeleteBuffers(1, &_buffer_id);
            }
        
            inline GLint buffer_id() const { return _buffer_id; }
            inline GLint vertex_count() const { return _vertex_count; }
        
            inline const std::vector<RenderDefinitionItem>& vertex_attribs() const { return _vertex_attribs; }
            inline const std::vector<unsigned short>& indices() const { return _indices; }
            inline GLint draw_mode() const { return _draw_mode; }
            inline GLsizei stride() const { return _stride; }
        private:
            std::vector<RenderDefinitionItem> _vertex_attribs;
            std::vector<unsigned short> _indices;
        
            GLuint _buffer_id;
            GLint _vertex_count;
        
            GLint _draw_mode;
        
            GLsizei _stride;
    };
    
    template <typename TPODVertexData>
    class RenderObject : public Drawable, private boost::noncopyable
    {
        public:
            RenderObject(const RenderDefinition<TPODVertexData>* definition,
                         const ShaderProgram* program, const GLTexture* texture = nullptr)
                : _program(program), _definition(definition), _texture(texture)
            {
                if (!(program->uniform_location("modelview", _modelview_location) &&
                      program->uniform_location("projection", _projection_location)))
                {
                    throw BadRenderObjectFormatException("RenderObject shaders must define 'modelview' and 'projection' uniforms.");
                }
                
                boost::unordered_set<std::string> attrib_set(program->attrib_names());
                for (RenderDefinitionItem item : definition->vertex_attribs())
                {
                    attrib_set.erase(item.name());
                }
                if (attrib_set.size() > 0)
                {
                    throw BadRenderObjectFormatException("Missing vertex attrib pointers: " + boost::algorithm::join(attrib_set, ", "));
                }
            }
        
            virtual ~RenderObject()
            {
            }
        
            virtual void draw(const glm::mat4& modelview, const glm::mat4& projection) const
            {
                _program->use_program();
                if (_texture != nullptr) _texture->bind();
                
                glBindBuffer(GL_ARRAY_BUFFER, _definition->buffer_id());
            
                apply_uniforms(modelview, projection);
                
                _program->enable_attrib_arrays();
                apply_vertex_pointers();
                
                glDrawElements(_definition->draw_mode(), _definition->vertex_count(), GL_UNSIGNED_SHORT, _definition->indices().data());
                
                _program->disable_attrib_arrays();
            }
        
            inline bool set_uniform(const std::string& name, const glm::vec2& v)
            {
                if (name == "modelview" || name == "projection" || (_texture != nullptr && name == "texture"))
                {
                    LOGOG_WARN("Attempted to set reserved uniform '%s' on a RenderObject. Ignored.", name.c_str());
                    return false;
                }
                
                _uniforms_vec2.insert(std::make_pair(name, v));
                return true;
            }
            inline bool set_uniform(const std::string& name, const glm::vec3& v)
            {
                if (name == "modelview" || name == "projection" || (_texture != nullptr && name == "texture"))
                {
                    LOGOG_WARN("Attempted to set reserved uniform '%s' on a RenderObject. Ignored.", name.c_str());
                    return false;
                }
                
                _uniforms_vec3.insert(std::make_pair(name, v));
                return true;
            }
            inline bool set_uniform(const std::string& name, const glm::vec4& v)
            {
                if (name == "modelview" || name == "projection" || (_texture != nullptr && name == "texture"))
                {
                    LOGOG_WARN("Attempted to set reserved uniform '%s' on a RenderObject. Ignored.", name.c_str());
                    return false;
                }
                
                _uniforms_vec4.insert(std::make_pair(name, v));
                return true;
            }
            inline bool set_uniform(const std::string& name, const glm::mat3& m)
            {
                if (name == "modelview" || name == "projection" || (_texture != nullptr && name == "texture"))
                {
                    LOGOG_WARN("Attempted to set reserved uniform '%s' on a RenderObject. Ignored.", name.c_str());
                    return false;
                }
                
                _uniforms_mat3.insert(std::make_pair(name, m));
                return true;
            }
            inline bool set_uniform(const std::string& name, const glm::mat4& m)
            {
                if (name == "modelview" || name == "projection" || (_texture != nullptr && name == "texture"))
                {
                    LOGOG_WARN("Attempted to set reserved uniform '%s' on a RenderObject. Ignored.", name.c_str());
                    return false;
                }
                _uniforms_mat4.insert(std::make_pair(name, m));
                return true;
            }
            inline bool set_uniform(const std::string& name, float val )
            {
                if (name == "modelview" || name == "projection" || (_texture != nullptr && name == "texture"))
                {
                    LOGOG_WARN("Attempted to set reserved uniform '%s' on a RenderObject. Ignored.", name.c_str());
                    return false;
                }
                
                _uniforms_float.insert(std::make_pair(name, val));
                return true;
            }
            inline bool set_uniform(const std::string& name, int val )
            {
                if (name == "modelview" || name == "projection" || (_texture != nullptr && name == "texture"))
                {
                    LOGOG_WARN("Attempted to set reserved uniform '%s' on a RenderObject. Ignored.", name.c_str());
                    return false;
                }
                
                _uniforms_int.insert(std::make_pair(name, val));
                return true;
            }
            inline bool set_uniform(const std::string& name, bool val )
            {
                if (name == "modelview" || name == "projection" || (_texture != nullptr && name == "texture"))
                {
                    LOGOG_WARN("Attempted to set reserved uniform '%s' on a RenderObject. Ignored.", name.c_str());
                    return false;
                }
                
                _uniforms_bool.insert(std::make_pair(name, val));
                return true;
            }
        protected:
            inline void apply_uniforms(const glm::mat4& modelview, const glm::mat4& projection) const
            {
                for (auto u : _uniforms_vec2) _program->set_uniform(u.first, u.second);
                for (auto u : _uniforms_vec3) _program->set_uniform(u.first, u.second);
                for (auto u : _uniforms_vec4) _program->set_uniform(u.first, u.second);
                for (auto u : _uniforms_mat3) _program->set_uniform(u.first, u.second);
                for (auto u : _uniforms_mat4) _program->set_uniform(u.first, u.second);
                for (auto u : _uniforms_float) _program->set_uniform(u.first, u.second);
                for (auto u : _uniforms_bool) _program->set_uniform(u.first, u.second);
                for (auto u : _uniforms_int) _program->set_uniform(u.first, u.second);
                
                glUniformMatrix4fv(_projection_location, 1, GL_FALSE, glm::value_ptr(projection));
                glUniformMatrix4fv(_modelview_location, 1, GL_FALSE, glm::value_ptr(modelview));
            }
        
            inline void apply_vertex_pointers() const
            {
                for (auto a : _definition->vertex_attribs())
                {
                    _program->vertex_attrib_pointer(a.name(), a.size(), a.type(), a.normalized(), _definition->stride(), a.offset());
                }
            }
        
            const ShaderProgram* _program;
            const GLTexture* _texture;
            const RenderDefinition<TPODVertexData>* _definition;
        private:
            GLint _modelview_location;
            GLint _projection_location;
        
            boost::unordered_map<std::string, glm::vec2> _uniforms_vec2;
            boost::unordered_map<std::string, glm::vec3> _uniforms_vec3;
            boost::unordered_map<std::string, glm::vec4> _uniforms_vec4;
            boost::unordered_map<std::string, glm::mat3> _uniforms_mat3;
            boost::unordered_map<std::string, glm::mat4> _uniforms_mat4;
            boost::unordered_map<std::string, float> _uniforms_float;
            boost::unordered_map<std::string, int> _uniforms_int;
            boost::unordered_map<std::string, bool> _uniforms_bool;
    };
}