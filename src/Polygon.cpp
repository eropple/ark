//
//  Polygon.cpp
//  Ark
//
//  Created by Ed Ropple on 1/18/13.
//  Copyright (c) 2013 Large Russian Games. All rights reserved.
//

#include <Ark/Polygon.h>
#include <Ark/math_utils.h>

#include <algorithm>
#include <limits>

namespace Ark
{
    bool Polygon::collide(const Polygon& other, glm::vec2& penetrate) const
    {
        std::vector<glm::vec2> contacts;
        
        for (auto iter = _vertices.begin(); iter != _vertices.end(); ++iter)
        {
            auto pointA = (*iter);
            auto pointB = *(iter + 1 == _vertices.end() ? _vertices.begin()
                                                        : iter + 1); // if at end of list, loop to start.
            
            glm::vec2 edge(pointB.x - pointA.x, pointB.y - pointA.y);
            glm::vec2 normal = glm::normalize(glm::vec2(-edge.y, edge.x));
            
            float threshhold = glm::dot(pointA, normal); // The distance of this edge along the normal axis
            
            float min_projected = std::numeric_limits<float>::infinity();
            
            for (auto o_iter = other._vertices.begin(); o_iter != other._vertices.end(); ++o_iter)
            {
                float projected = glm::dot(*o_iter, normal);
                min_projected = std::min(min_projected, projected);
            }
            
            if (min_projected < threshhold) contacts.push_back(normal * -min_projected);
        }
    
        if (contacts.size() == _vertices.size())
        {
            glm::vec2 p;
            penetrate = *(std::min_element(contacts.begin(), contacts.end(), glm_vector_comparator));
            return true;
        }
    
        return false;
    }
}