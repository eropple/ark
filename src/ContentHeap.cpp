//
//  ContentHeap.cpp
//  Ark
//
//  Created by Ed Ropple on 12/26/12.
//  Copyright (c) 2012 Large Russian Games. All rights reserved.
//

#include <Ark/file_utils.h>
#include <Ark/ContentHeap.h>
#include <Ark/Exception.h>

#include <boost/algorithm/string.hpp>

#include <logog/logog.hpp>

#include <physfs.h>

namespace Ark
{
    namespace fs = boost::filesystem;
    
    // TODO: replace with std::atomic when libstdc++ catches up
    unsigned int heap_number = 0;

    ContentHeap::ContentHeap(ContentHeap* parent_heap)
        : _parent(parent_heap), _heap_number(heap_number++),
          _scale_factor(parent_heap == nullptr ? 1.0f : parent_heap->_scale_factor)
    {
        if (parent_heap != nullptr)
        {
            LOGOG_DEBUG("Heap %d: constructed (parent: %d).", _heap_number, parent_heap->heap_id());
        }
        else
        {
            LOGOG_DEBUG("Heap %d: constructed (is root heap).", _heap_number);
        }
    }
    
    ContentHeap::~ContentHeap()
    {
        if (_parent != nullptr) _parent->remove_child(this);
        
        std::vector<ContentHeap*> temp = child_heaps; // because we're going to delete the real list
        for (auto child_heap : temp) delete child_heap;
    
        for (auto bitmap : memory_bitmaps) delete bitmap.second;
        for (auto script : script_modules) delete script.second;
        
        LOGOG_DEBUG("Heap %d: destroyed.", _heap_number);
    }
    
    ContentHeap* ContentHeap::create_child_heap()
    {
        auto heap = new ContentHeap(this);
        add_child_heap(heap);
        return heap;
    }
    
    void ContentHeap::remove_child(ContentHeap* child_heap)
    {
        for (auto iter = child_heaps.begin(); iter != child_heaps.end(); ++iter)
        {
            if ((*iter) == child_heap)
            {
                child_heaps.erase(iter);
                return;
            }
        }
    }
    
    GLTexture* ContentHeap::load_texture(const std::string& key)
    {
        GLTexture* texture = get_texture_if_loaded(key);
        if (texture != nullptr) return texture;
        
        if (_parent != nullptr)
        {
            texture = _parent->get_texture_if_loaded(key);
            if (texture != nullptr) return texture;
        }
        LOGOG_DEBUG("Heap %d: '%s' not found, loading into cache.", _heap_number, key.c_str());
        
        texture = load_texture_unmanaged(key);
        textures.insert(std::make_pair(key, texture));
        return texture;
    }
    GLTexture* ContentHeap::get_texture_if_loaded(const std::string& key) const
    {
        auto iter = textures.find(key);
        if (iter != textures.end())
        {
            LOGOG_DEBUG("Heap %d: '%s' requested, already cached.", _heap_number, key.c_str());
            return iter->second;
        }
        
        return nullptr;
    }
    GLTexture* ContentHeap::load_texture_unmanaged(const std::string& key) const
    {
        if (scale_factor() == 2.0f)
        {
            std::string retina_key = key;
            boost::replace_last(retina_key, ".png", "-sd.png");
            return new GLTexture(PHYSFS_exists(retina_key.c_str()) ? retina_key : key);
        }
        else
        {
            return new GLTexture(key);
        }
    }
    
    
    
    ScriptModule* ContentHeap::load_script_module(const std::string& key)
    {
        ScriptModule* script = get_script_module_if_loaded(key);
        if (script != nullptr) return script;
        
        if (_parent != nullptr)
        {
            script = _parent->get_script_module_if_loaded(key);
            if (script != nullptr) return script;
        }
        LOGOG_DEBUG("Heap %d: '%s' not found, loading into cache.", _heap_number, key.c_str());
        
        script = load_script_module_unmanaged(key);
        script_modules.insert(std::make_pair(key, script));
        return script;
    }
    
    ScriptModule* ContentHeap::get_script_module_if_loaded(const std::string& key) const
    {
        auto iter = script_modules.find(key);
        if (iter != script_modules.end())
        {
            LOGOG_DEBUG("Heap %d: '%s' requested, already cached.", _heap_number, key.c_str());
            return iter->second;
        }
        
        return nullptr;
    }
    
    ScriptModule* ContentHeap::load_script_module_unmanaged(const std::string& key) const
    {
        boost::unordered_map<std::string, PHYSFS_File*> files;
        PHYSFS_File* file = nullptr;
        if (!open_file(key, &file))
        {
            throw ContentException("Couldn't find script file: " + key);
            return nullptr;
        }
        
        files.insert(std::make_pair(key, file));
        auto script = new ScriptModule(files);
        PHYSFS_close(file);
        return script;
    }
    
    
    
    MemoryBitmap* ContentHeap::load_memory_bitmap(const std::string& key)
    {
        MemoryBitmap* bitmap = get_memory_bitmap_if_loaded(key);
        if (bitmap != nullptr) return bitmap;
        
        if (_parent != nullptr)
        {
            bitmap = _parent->get_memory_bitmap_if_loaded(key);
            if (bitmap != nullptr) return bitmap;
        }
        LOGOG_DEBUG("Heap %d: '%s' not found, loading into cache.", _heap_number, key.c_str());
        
        bitmap = load_memory_bitmap_unmanaged(key);
        memory_bitmaps.insert(std::make_pair(key, bitmap));
        return bitmap;
    }
    
    MemoryBitmap* ContentHeap::get_memory_bitmap_if_loaded(const std::string& key) const
    {
        auto iter = memory_bitmaps.find(key);
        if (iter != memory_bitmaps.end())
        {
            LOGOG_DEBUG("Heap %d: '%s' requested, already cached.", _heap_number, key.c_str());
            return iter->second;
        }
        
        return nullptr;
    }
    
    MemoryBitmap* ContentHeap::load_memory_bitmap_unmanaged(const std::string& key) const
    {
        PHYSFS_File* file = nullptr;
        if (!open_file(key, &file))
        {
            throw ContentException("Couldn't find memory bitmap: " + key);
        }
        
        auto bitmap = new MemoryBitmap(file);
        PHYSFS_close(file);
        return bitmap;
    }
}