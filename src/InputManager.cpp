// InputManager.cpp - implementation for InputManager
#include <Ark/InputManager.h>
#include <Ark/StateManager.h>

#include <GL/glfw3.h>

namespace Ark
{
    InputManager* singleton_instance = nullptr;

    void InputManager::initialize(GLFWwindow* window)
    {
        singleton_instance = new InputManager(window);
    }
    
    
    void InputManager::teardown()
    {
        assert( singleton_instance != nullptr );
        
        delete singleton_instance;
    }
    
    InputManager* InputManager::instance()
    {
        assert( singleton_instance != nullptr );
        
        return singleton_instance;
    }
    
    
    
    
    
    
    InputManager::InputManager(GLFWwindow* window)
        : _window(window), last_x(0), last_y(0), last_wheel_position(0)
    {
#ifdef DESKTOP
        glfwSetMouseButtonCallback(window, glfw_mouse_button_callback);
        glfwSetCursorPosCallback(window, glfw_cursor_position_callback);
        glfwSetScrollCallback(window, glfw_scroll_callback);
        glfwSetKeyCallback(window, glfw_key_callback);
        glfwSetCharCallback(window, glfw_char_callback);
#endif
    }

    InputManager::~InputManager()
    {
#ifdef DESKTOP
        glfwSetMouseButtonCallback(_window, nullptr);
        glfwSetCursorPosCallback(_window, nullptr);
        glfwSetScrollCallback(_window, nullptr);
        glfwSetKeyCallback(_window, nullptr);
        glfwSetCharCallback(_window, nullptr);
#endif
    }

    bool InputManager::is_key_pressed(int key_identifier) const
    {
        return (glfwGetKey(_window, key_identifier) == GLFW_PRESS);
    }


    void InputManager::key_handler(int key_identifier, bool pressed)
    {
        StateManager::instance()->key_handler(key_identifier, pressed);
    }
    void InputManager::char_handler(int unichar_identifier)
    {
        StateManager::instance()->char_handler(unichar_identifier);
    }

    void InputManager::mouse_position_handler(int x, int y)
    {
        int dX = x - last_x;
        int dY = y - last_y;

        last_x = x;
        last_y = y;

        StateManager::instance()->mouse_position_handler(x, y, dX, dY);
    }


    void InputManager::mouse_button_handler(int button, bool pressed)
    {
        StateManager::instance()->mouse_button_handler(button, last_x, last_y, pressed);
    }
    void InputManager::mouse_wheel_handler(double wheel_position)
    {
        double delta = wheel_position - last_wheel_position;
        last_wheel_position = wheel_position;

        int tick = 0;
        if (delta > 0) tick = 1;
        if (delta < 0) tick = -1;

        StateManager::instance()->mouse_wheel_handler(wheel_position, tick, last_x, last_y);
    }


#ifdef DESKTOP
    void InputManager::glfw_mouse_button_callback(GLFWwindow* window, int button, int action)
    {
        singleton_instance->mouse_button_handler(button, action == GLFW_PRESS);
    }

    void InputManager::glfw_cursor_position_callback(GLFWwindow* window, int x, int y)
    {
        singleton_instance->mouse_position_handler(x, y);
    }

    void InputManager::glfw_scroll_callback(GLFWwindow* window, double x, double y)
    {
        singleton_instance->mouse_wheel_handler(y);
    }

    void InputManager::glfw_key_callback(GLFWwindow* window, int key, int action)
    {
        singleton_instance->key_handler(key, action == GLFW_PRESS);
    }

    void InputManager::glfw_char_callback(GLFWwindow* window, int character)
    {
        singleton_instance->char_handler(character);
    }
#endif
}