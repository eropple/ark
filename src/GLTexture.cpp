//
//  GLTexture.cpp
//  Ark
//
//  Created by Ed Ropple on 1/24/13.
//  Copyright (c) 2013 Large Russian Games. All rights reserved.
//

#include <Ark/ContentHeap.h>
#include <Ark/file_utils.h>
#include <Ark/GLTexture.h>
#include <Ark/Exception.h>

#include <logog/logog.hpp>

namespace Ark
{
    class ContentHeap;

    GLTexture::GLTexture(const std::string& key)
        : _reload_key(key), _reloadable(true)
    {
        reload(false);
    }
    GLTexture::GLTexture(const MemoryBitmap* bitmap)
        : _reloadable(false)
    {
        load_texture(bitmap);
    }
    
    void GLTexture::bind(GLuint texture_unit) const
    {
        glActiveTexture(texture_unit);
        glBindTexture(GL_TEXTURE_2D, _texture_id);
    }
    void GLTexture::bind_inactive(GLuint texture_unit) const
    {
        GLint tex;
        glGetIntegerv(GL_ACTIVE_TEXTURE, &tex);
        
        glActiveTexture(texture_unit);
        glBindTexture(GL_TEXTURE_2D, _texture_id);
        
        glActiveTexture(tex);
    }
    
    void GLTexture::reload(bool dispose_first)
    {
        if (_reloadable == false)
        {
            throw GLException("Attempted to reload an unreloadable GLTexture.");
        }
        
        if (dispose_first) delete_texture();
        
        PHYSFS_File* file = nullptr;
        if (!open_file(_reload_key, &file))
        {
            throw ContentException("Couldn't find memory bitmap: " + _reload_key);
        }
        
        
        MemoryBitmap* bitmap = new MemoryBitmap(file);
        load_texture(bitmap);
        PHYSFS_close(file);
        delete bitmap;
    }
    
    void GLTexture::load_texture(const MemoryBitmap *bitmap)
    {
        glEnable(GL_TEXTURE_2D);
        glGenTextures(1, &_texture_id);
        glBindTexture(GL_TEXTURE_2D, _texture_id);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        
        _bounds = FloatRect(0, 0, bitmap->width(), bitmap->height());
        
        glTexImage2D(GL_TEXTURE_2D, // target
                     0,  // level, 0 = base, no minimap,
                     GL_RGBA, // internalformat
                     bitmap->width(),  // width
                     bitmap->height(),  // height
                     0,  // border, always 0 in OpenGL ES
                     GL_RGBA,  // format
                     GL_UNSIGNED_BYTE, // type
                     bitmap->data().data());
        
        LOGOG_DEBUG("Loaded %dx%d bitmap into GL texture slot %d.", bitmap->width(), bitmap->height(), _texture_id);
    }
    
    void GLTexture::delete_texture()
    {
        glEnable(GL_TEXTURE_2D);
        glDeleteTextures(1, &_texture_id);
    }
}