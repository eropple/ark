//
//  MemoryBitmap.cpp
//  Ark
//
//  Created by Ed Ropple on 1/24/13.
//  Copyright (c) 2013 Large Russian Games. All rights reserved.
//

#include <Ark/Exception.h>
#include <Ark/MemoryBitmap.h>
#include <Ark/file_utils.h>
#include <Ark/LodePNG.h>

#include <boost/lexical_cast.hpp>

#include <logog/logog.hpp>

#include <physfs.h>

namespace Ark
{
    MemoryBitmap::MemoryBitmap(PHYSFS_File* file)
    {
        std::vector<char> vec;
        read_vector_from_file(file, vec);
        
        lodepng::State state;
        auto retval = lodepng::decode(_data, _width, _height, (unsigned char*)&vec[0], vec.size());
        
        if (retval)
        {
            throw ContentException("LodePNG failed to load image: " + std::string(lodepng_error_text(retval)) +
                                   " (error " + boost::lexical_cast<std::string>(retval) + ")");
        }
    }
    
    MemoryBitmap::MemoryBitmap(unsigned int width, unsigned int height, const Color4& color)
    {
        _width = width;
        _height = height;
    
        size_t pixels = width * height;
    
        unsigned char r = color.r_byte();
        unsigned char g = color.g_byte();
        unsigned char b = color.b_byte();
        unsigned char a = color.a_byte();
    
        _data.reserve(pixels * 4);
        
        for (size_t i = 0; i < pixels; ++i)
        {
            _data.push_back(r);
            _data.push_back(g);
            _data.push_back(b);
            _data.push_back(a);
        }
    }
    
    
    MemoryBitmap* MemoryBitmap::copy_slice(unsigned int x, unsigned int y, unsigned int width, unsigned int height)
    {
        MemoryBitmap* child = new MemoryBitmap(width, height, Color4());
        
        for (unsigned int y2 = 0; y2 < height; ++y2)
        {
            for (unsigned int x2 = 0; x2 < width; ++x2)
            {
                child->set_color(x2, y2, get(x2 + x, y2 + y));
            }
        }
        
        return child;
    }
}