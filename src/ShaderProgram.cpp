//
//  ShaderProgram.cpp
//  Ark
//
//  Created by Ed Ropple on 1/26/13.
//  Copyright (c) 2013 Large Russian Games. All rights reserved.
//

#include <Ark/ShaderProgram.h>
#include <Ark/file_utils.h>

#include <logog/logog.hpp>

#include <glm/gtc/type_ptr.hpp>

#include <yaml-cpp/yaml.h>

namespace Ark
{
    bool build_shader(GLuint shader_type, PHYSFS_File* file, GLuint& shader_id);
    bool link_shader(const std::vector<GLuint>& shader_ids, GLuint& program_id);
    void cleanup_shaders(GLuint program_id, const std::vector<GLuint>& shader_ids);
    void process_all_names(int program_id, boost::unordered_set<std::string>& uniform_names,
                           boost::unordered_set<std::string>& attrib_names);
    
    

    ShaderProgram::ShaderProgram(const std::vector<PHYSFS_File*>& vertex_shaders,
                                 const std::vector<PHYSFS_File*>& fragment_shaders)
    {
    
        std::vector<GLuint> shader_ids;
        
        for (auto shader_file : vertex_shaders)
        {
            GLuint shader_id;
            if (!build_shader(GL_VERTEX_SHADER, shader_file, shader_id))
            {
                throw new GLException("Shader failed to build; check the log for the error.");
            }
            shader_ids.push_back(shader_id);
        }
        for (auto shader_file : fragment_shaders)
        {
            GLuint shader_id;
            if (!build_shader(GL_FRAGMENT_SHADER, shader_file, shader_id))
            {
                throw new GLException("Shader failed to build; check the log for the error.");
            }
            shader_ids.push_back(shader_id);
        }
        
        
        if (!(link_shader(shader_ids, _program_id)))
        {
            throw new GLException("Shader failed to link; check the log for the error.");
        }
        
        cleanup_shaders(_program_id, shader_ids);
        
        boost::unordered_set<std::string> uniforms;
        boost::unordered_set<std::string> attribs;
        
        // populates the name sets
        process_all_names(_program_id, uniforms, attribs);
        
        
        for (auto uniform : uniforms)
        {
            GLint location = glGetUniformLocation(_program_id, uniform.c_str());
            if (location == -1)
            {
                LOGOG_WARN("Shader program does not have a uniform named '%s'. Omitting from uniform list.", uniform.c_str());
            }
            
            _uniform_locations.insert(std::make_pair(uniform, location));
        }
        
        for (auto attrib : attribs)
        {
            GLint location = glGetAttribLocation(_program_id, attrib.c_str());
            if (location == -1)
            {
                LOGOG_WARN("Shader program does not have an attribute named '%s'. Omitting from attrib list.", attrib.c_str());
            }
            
            _attribute_locations.insert(std::make_pair(attrib, location));
        }
    }
    
    ShaderProgram::~ShaderProgram()
    {
        glDeleteProgram(_program_id);
    }
    
    bool ShaderProgram::uniform_location(const std::string& name, GLint& location) const
    {
        auto iter = _uniform_locations.find(name);
        if (iter != _uniform_locations.end())
        {
            location = iter->second;
            return true;
        }
        return false;
    }
    bool ShaderProgram::attrib_location(const std::string& name, GLint& location) const
    {
        auto iter = _attribute_locations.find(name);
        if (iter != _attribute_locations.end())
        {
            location = iter->second;
            return true;
        }
        return false;
    }
    
    
    void ShaderProgram::use_program() const
    {
        glUseProgram(_program_id);
    }
    
    boost::unordered_set<std::string> ShaderProgram::uniform_names() const
    {
        boost::unordered_set<std::string> set;
        for (auto pair : _uniform_locations) set.insert(pair.first);
        return set;
    }
    boost::unordered_set<std::string> ShaderProgram::attrib_names() const
    {
        boost::unordered_set<std::string> set;
        for (auto pair : _attribute_locations) set.insert(pair.first);
        return set;
    }
    
    void ShaderProgram::enable_attrib_arrays() const
    {
        for (auto attrib : _attribute_locations) glEnableVertexAttribArray(attrib.second);
    }
    void ShaderProgram::disable_attrib_arrays() const
    {
        for (auto attrib : _attribute_locations) glDisableVertexAttribArray(attrib.second);
    }
    
    bool ShaderProgram::vertex_attrib_pointer(const std::string& name,
                                      GLint size, GLenum type, GLboolean normalized,
                                      GLsizei stride, int offset) const
    {
        auto iter = _attribute_locations.find(name);
        if (iter == _attribute_locations.end())
        {
            LOGOG_WARN("Attribute '%s' not found. No operation.", name.c_str());
            return false;
        }
        
        glVertexAttribPointer(iter->second, size, type, normalized, stride, (GLvoid*)offset);
        return true;
    }
    
    
    bool ShaderProgram::set_uniform(const std::string& name, const glm::vec2& v) const
    {
        auto iter = _uniform_locations.find(name);
        if (iter == _uniform_locations.end())
        {
            LOGOG_WARN("Uniform '%s' not found. Discarding argument.", name.c_str());
            return false;
        }
        
        glUniform2fv(iter->second, 1, glm::value_ptr(v));
        
        return true;
    }
    bool ShaderProgram::set_uniform(const std::string& name, const glm::vec3& v) const
    {
        auto iter = _uniform_locations.find(name);
        if (iter == _uniform_locations.end())
        {
            LOGOG_WARN("Uniform '%s' not found. Discarding argument.", name.c_str());
            return false;
        }
        
        glUniform3fv(iter->second, 1, glm::value_ptr(v));
        
        return true;
    }
    bool ShaderProgram::set_uniform(const std::string& name, const glm::vec4& v) const
    {
        auto iter = _uniform_locations.find(name);
        if (iter == _uniform_locations.end())
        {
            LOGOG_WARN("Uniform '%s' not found. Discarding argument.", name.c_str());
            return false;
        }
        
        glUniform4fv(iter->second, 1, glm::value_ptr(v));
        
        return true;
    }
    bool ShaderProgram::set_uniform(const std::string& name, const glm::mat3& m) const
    {
        auto iter = _uniform_locations.find(name);
        if (iter == _uniform_locations.end())
        {
            LOGOG_WARN("Uniform '%s' not found. Discarding argument.", name.c_str());
            return false;
        }
        
        glUniformMatrix3fv(iter->second, 1, GL_FALSE, glm::value_ptr(m));
        
        return true;
    }
    bool ShaderProgram::set_uniform(const std::string& name, const glm::mat4& m) const
    {
        auto iter = _uniform_locations.find(name);
        if (iter == _uniform_locations.end())
        {
            LOGOG_WARN("Uniform '%s' not found. Discarding argument.", name.c_str());
            return false;
        }
        
        glUniformMatrix4fv(iter->second, 1, GL_FALSE, glm::value_ptr(m));
        
        return true;
    }
    bool ShaderProgram::set_uniform(const std::string& name, float val ) const
    {
        auto iter = _uniform_locations.find(name);
        if (iter == _uniform_locations.end())
        {
            LOGOG_WARN("Uniform '%s' not found. Discarding argument.", name.c_str());
            return false;
        }
        
        glUniform1f(iter->second, val);
        
        return true;
    }
    bool ShaderProgram::set_uniform(const std::string& name, int val ) const
    {
        auto iter = _uniform_locations.find(name);
        if (iter == _uniform_locations.end())
        {
            LOGOG_WARN("Uniform '%s' not found. Discarding argument.", name.c_str());
            return false;
        }
        
        glUniform1i(iter->second, val);
        
        return true;
    }
    bool ShaderProgram::set_uniform(const std::string& name, bool val ) const
    {
        auto iter = _uniform_locations.find(name);
        if (iter == _uniform_locations.end())
        {
            LOGOG_WARN("Uniform '%s' not found. Discarding argument.", name.c_str());
            return false;
        }
        
        glUniform1i(iter->second, val);
        
        return true;
    }
    
    
    
    
    
    ShaderProgram* ShaderProgram::from_yaml(const std::string& key)
    {
        PHYSFS_File* file;
        bool success = Ark::open_file(key, &file);
        if (success == false)
        {
            throw ContentException("Failed to open " + key + " for reading shader YAML.");
        }
    
        YAML::Node document;
        if (!read_yaml_from_file(file, document))
        {
            throw ContentException("Failed to parse shader YAML.");
        }
        PHYSFS_close(file);
        
        std::vector<PHYSFS_File*> vertex_shaders;
        std::vector<PHYSFS_File*> fragment_shaders;
        
        auto vertex_keys = document["vertex_shaders"].as<std::vector<std::string> >();
        for (auto key : vertex_keys)
        {
            PHYSFS_File* shader_file;
            bool success = Ark::open_file(key, &shader_file);
            if (success == false)
            {
                throw ContentException("Failed to open " + key + " for reading shader program.");
            }
            vertex_shaders.push_back(shader_file);
        }
        
        auto fragment_keys = document["fragment_shaders"].as<std::vector<std::string> >();
        for (auto key : fragment_keys)
        {
            PHYSFS_File* shader_file;
            bool success = Ark::open_file(key, &shader_file);
            if (success == false)
            {
                throw ContentException("Failed to open " + key + " for reading shader program.");
            }
            fragment_shaders.push_back(shader_file);
        }
        
        auto program = new ShaderProgram(vertex_shaders, fragment_shaders);
        
        for (auto file : vertex_shaders) PHYSFS_close(file);
        for (auto file : fragment_shaders) PHYSFS_close(file);
        
        return program;
    }
    
    
    
    bool build_shader(GLuint shader_type, PHYSFS_File* file, GLuint& shader_id)
    {
        shader_id = glCreateShader(shader_type);
        
        std::string str;
        read_string_from_file(file, str);
        const char* c = &str[0];
        int s = (int) strlen(c);
        
        assert (glShaderSource != nullptr);
        
        glShaderSource(shader_id, 1, &c, &s);
        glCompileShader(shader_id);
        
        int infologLength = 0;
        glGetShaderiv(shader_id, GL_INFO_LOG_LENGTH, &infologLength);
        
        if (infologLength > 0)
        {
            char* infoLog = new char[infologLength];
            int charsWritten  = 0;
            glGetShaderInfoLog(shader_id, infologLength, &charsWritten, infoLog);
            LOGOG_ERROR("Failed to compile shader: %s", infoLog);
            delete[] infoLog;
            LOGOG_DEBUG("Shader: \n%s", str.c_str());
            return false;
        }
        
        return true;
    }
    
    bool link_shader(const std::vector<GLuint>& shader_ids, GLuint& program_id)
    {
        program_id = glCreateProgram();
        
        for (auto shader_id : shader_ids) glAttachShader(program_id, shader_id);
        
        glLinkProgram(program_id);
        
        int infologLength = 0;
        glGetProgramiv(program_id, GL_INFO_LOG_LENGTH, &infologLength);
        
        if (infologLength > 0)
        {
            char* infoLog = new char[infologLength];
            int charsWritten  = 0;
            glGetShaderInfoLog(program_id, infologLength, &charsWritten, infoLog);
            LOGOG_ERROR("Failed to link program: %s", infoLog);
            delete[] infoLog;
            return false;
        }
        
        return true;
    }
    
    void cleanup_shaders(GLuint program_id, const std::vector<GLuint>& shader_ids)
    {
        for (auto shader_id : shader_ids)
        {
            glDetachShader(program_id, shader_id);
            glDeleteShader(shader_id);
        }
    }
    
    
    
    void process_all_names(int program_id, boost::unordered_set<std::string>& uniform_names,
                           boost::unordered_set<std::string>& attrib_names)
    {
        // http://stackoverflow.com/a/12611619
        // GL_ACTIVE_UNIFORM_MAX_LENGTH is buggy on some drivers; 256 should be enough for any
        // identifier we're likely to ever see. If not, shorten your damn identifiers.
        int buffer_size = 256;
        
        GLchar* buffer = new GLchar[buffer_size];
        
        GLint numActiveAttribs = 0;
        GLint numActiveUniforms = 0;
        glGetProgramiv(program_id, GL_ACTIVE_ATTRIBUTES, &numActiveAttribs);
        glGetProgramiv(program_id, GL_ACTIVE_UNIFORMS, &numActiveUniforms);
        
        for(int attrib = 0; attrib < numActiveAttribs; ++attrib)
        {
            GLint arraySize = 0;
            GLenum type = 0;
            GLsizei actualLength = 0;
            glGetActiveAttrib(program_id, attrib, buffer_size, &actualLength, &arraySize, &type, buffer);
            std::string name(buffer, actualLength);
            
            attrib_names.insert(name);
        }
        
        for(int unif = 0; unif < numActiveUniforms; ++unif)
        {
            GLint arraySize = 0;
            GLenum type = 0;
            GLsizei actualLength = 0;
            glGetActiveUniform(program_id, unif, buffer_size, &actualLength, &arraySize, &type, buffer);
            std::string name(buffer, actualLength);
            
            uniform_names.insert(name);
        }
        
        delete buffer;
    }
}
