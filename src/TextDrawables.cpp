//
//  TextDrawables.cpp
//  Ark
//
//  Created by Ed Ropple on 3/15/13.
//  Copyright (c) 2013 Large Russian Games. All rights reserved.
//

#include <Ark/TextDrawables.h>
#include <Ark/SimpleTexturedQuad.h>
#include <Ark/FontDescription.h>

#include <glm/gtc/matrix_transform.hpp>

namespace Ark
{
    TextDrawable::TextDrawable(const std::vector<LineDrawable*> lines)
        : _lines(lines) {}
    
    TextDrawable::~TextDrawable()
    {
        for (auto line : _lines) delete line;
    }
    
    void TextDrawable::draw(const glm::mat4& modelview, const glm::mat4& projection) const
    {
        for (auto line : _lines) line->draw(modelview, projection);
    }
    
    
    LineDrawable::LineDrawable(const glm::vec3 offset, const FontDescription* font, const std::vector<GlyphDrawable*> glyphs)
        : _offset(offset), _font(font), _glyphs(glyphs)
    {
        
    }
    LineDrawable::~LineDrawable()
    {
        for (auto glyph : _glyphs) delete glyph;
    }
    
    void LineDrawable::draw(const glm::mat4& modelview, const glm::mat4& projection) const
    {
        float xadv = 0;
        auto translated = glm::translate(modelview, _offset);
        
        for (auto glyph : _glyphs)
        {
            auto character = glyph->character();
            glyph->draw(glm::translate(translated,
                                       glm::vec3(0.0f, _font->baseline() - character->rect().height, 0.0f)),
                        projection);
            
            translated = glm::translate(translated, glm::vec3(glyph->character()->xadvance(), 0, 0));
            xadv += character->xadvance();
#if DEBUG
            LOGOG_DEBUG("xadv for '%d': %d (now %d)", character->letter(), character->xadvance(), xadv);
#endif
        }
    }
    
    GlyphDrawable::GlyphDrawable(const FontCharacter* character, const Color4& color, SimpleTexturedQuad* glyph_quad)
        : _character(character), _color(color), _glyph_quad(glyph_quad)
    {
        
    }
    GlyphDrawable::~GlyphDrawable()
    {
        
    }
    
    void GlyphDrawable::draw(const glm::mat4& modelview, const glm::mat4& projection) const
    {
        _glyph_quad->set_color(_color);
        _glyph_quad->draw(modelview, projection);
    }
}
