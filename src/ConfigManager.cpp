//
//  ConfigManager.cpp
//  Ark
//
//  Created by Ed Ropple on 12/26/12.
//  Copyright (c) 2012 Large Russian Games. All rights reserved.
//

#include <Ark/ConfigManager.h>
#include <Ark/utf8_utils.h>

#include <boost/algorithm/string.hpp>

#include <logog/logog.hpp>

namespace Ark
{
    ConfigManager* config_instance = nullptr;
    
    ConfigManager::ConfigManager(const std::vector<std::string>& args,
                          const std::vector<boost::filesystem::path>& config_search_paths)
    {
        namespace po = boost::program_options;

        std::vector<const char*> args_c;
        for (auto arg : args)
        {
            args_c.push_back(arg.c_str());
        }

        po::options_description opts;
                
        try
        {
            po::store(po::parse_command_line<char>(args_c.size(), &args_c[0], opts), vm);
        } catch (...) {}
                
        for (auto path : config_search_paths)
        {
            LOGOG_DEBUG("search path: %s", path.string().c_str());
            auto p = path / "settings.conf";
            if (!conf_exists(p)) continue;
            po::store(po::parse_config_file<char>(p.string().c_str(), opts, true), vm);
        }
                
        po::notify(vm);
        
        std::string v = this->get<std::string>("mod.names", "FakeDLC Core");
        boost::algorithm::split(_mod_names, v, boost::algorithm::is_any_of(" "));
    }



    void ConfigManager::initialize(const std::vector<std::string>& args,
                                   const std::vector<boost::filesystem::path>& config_search_paths)
    {
        assert( config_instance == nullptr );
        config_instance = new ConfigManager(args, config_search_paths);
    }
    void ConfigManager::teardown()
    {
        assert( config_instance != nullptr );
        delete config_instance;
    }
    
    const ConfigManager* ConfigManager::instance()
    {
        assert( config_instance != nullptr );
        return config_instance;
    }
}