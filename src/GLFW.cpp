//
//  GLFW.cpp
//  Ark
//
//  Created by Ed Ropple on 2/12/13.
//  Copyright (c) 2013 Large Russian Games. All rights reserved.
//

#include <Ark/GL.h>
#include <Ark/GLFW.h>
#include <Ark/Init.h>
#include <Ark/ConfigManager.h>
#include <Ark/InputManager.h>
#include <Ark/StateManager.h>

#include <cassert>

#include <glm/gtx/transform.hpp>

#if WIN32
    #include <Windows.h>
#endif

#ifdef DESKTOP

namespace Ark
{
    GLFW* glfw_instance = nullptr;
    bool glfw_initialized = false;
    
    void GLFW::initialize(const InitArgs& args)
    {
        assert( glfw_instance == nullptr );
        
        glfw_instance = new GLFW(args);
    }
    void GLFW::teardown()
    {
        assert( glfw_instance != nullptr );
        
        delete glfw_instance;
    }
    
    GLFW* GLFW::instance()
    {
        return glfw_instance;
    }

    GLFW::GLFW(const InitArgs& args)
    {
        if (glfw_initialized == false)
        {
            glfwInit();
            glfwSetErrorCallback(GLFW::glfw_error_callback);
        }

        auto config = ConfigManager::instance();

        _window_bounds = glm::ivec2(config->get<int>("window.width", 1280),
                                    config->get<int>("window.height", 720));
        bool fullscreen = config->get<bool>("window.fullscreen", false);

        GLFWmonitor* monitor = nullptr;

        if (fullscreen)
        {
            int monitor_count;
            GLFWmonitor** monitors = glfwGetMonitors(&monitor_count);

            int target_monitor = config->get<int>("window.fullscreen.target", -1);

            monitor = (target_monitor >= monitor_count || target_monitor == -1) ? glfwGetPrimaryMonitor()
                                                                                : monitors[target_monitor];
        }

        glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 2);
        glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 0);

//        glfwWindowHint(GLFW_HIDPI_IF_AVAILABLE, GL_TRUE);
        glfwWindowHint(GLFW_RESIZABLE, args.resizable() ? GL_TRUE : GL_FALSE);

        _window = glfwCreateWindow(_window_bounds.x, _window_bounds.y, args.application_name().c_str(),
                                   monitor, nullptr);

        glfwSetWindowSizeCallback(_window, GLFW::glfw_resize_callback);

        glfwMakeContextCurrent(_window);
        glfwSwapInterval(1);
        
        glewInit(); // must come after context initialization.
        
        InputManager::initialize(_window);
        
        glfwShowWindow(_window);
    }

    GLFW::~GLFW()
    {
    }
    
    void GLFW::ortho_projection(glm::mat4& mtx) const
    {
        mtx = glm::ortho(0.0f, (float)_window_bounds.x, (float)_window_bounds.y, 0.0f, -1000000.0f, 1000000.0f);
    }
    
    void GLFW::set_default_viewport()
    {
        float scale = scale_factor();
        glViewport(0, 0, (int)(_window_bounds.x * scale), (int)(_window_bounds.y * scale));
    }
    
    float GLFW::scale_factor() const
    {
        float scale;
        glfwGetWindowScale(_window, &scale);
        return scale;
    }
    
    
    void GLFW::swap_buffers() const
    {
        glfwSwapBuffers(_window);
    }
    

    void GLFW::glfw_error_callback(int error, const char* description)
    {
        LOGOG_CRITICAL("GLFW failure %d: %s", error, description);
#if WIN32
    #if !DEBUG 
        system("pause");
    #else
        MessageBoxA(nullptr, "GLFW failure", description, MB_OK);
    #endif
#endif
        exit(EXIT_FAILURE);
    }
    
    void GLFW::glfw_resize_callback(GLFWwindow* window, int width, int height)
    {
        float scale;
        glfwGetWindowScale(window, &scale);
        
        LOGOG_DEBUG("resizing: %d x %d (scale %f)", width, height, scale);
        
        // Setup viewport
        glViewport(0, 0, (int)(width * scale), (int)(height * scale));
        
        glm::mat4 projection;
        glfw_instance->ortho_projection(projection);
        
        glClearColor(0,0,0,0);
        glClear(GL_COLOR_BUFFER_BIT);
        state_manager()->draw(0, projection);
        glfwSwapBuffers(window);
    }

    

}
#endif