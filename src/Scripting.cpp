//
//  Scripting.cpp
//  Ark
//
//  Created by Ed Ropple on 1/17/13.
//  Copyright (c) 2013 Large Russian Games. All rights reserved.
//

#include <Ark/Scripting.h>
#include <Ark/Init.h>

#include <AngelScript/scriptstdstring.h>
#include <logog/logog.hpp>

#include <glm/glm.hpp>
#include <glm/gtc/type_precision.hpp>

#include <cassert>

#include <boost/format.hpp>

namespace {
    
    template<typename VT>
    const char* VectorTypeName() { throw std::runtime_error("vector type name not defined"); }
    template<> const char* VectorTypeName<glm::vec2>() { return "vec2"; }
    template<> const char* VectorTypeName<glm::i8vec2>() { return "i8vec2"; }
    template<> const char* VectorTypeName<glm::i32vec2>() { return "i32vec2"; }
    
    void print(std::string &msg)
    {
        Ark::scripting()->engine()->WriteMessage("Test", 0, 0, asMSGTYPE_INFORMATION, msg.c_str());
    }

    template<typename VT>
    std::string VectorToString(const VT &v)
    {
        return (boost::format("%1%(%2%, %3%)") % VectorTypeName<VT>() % v.x % v.y).str();
    }
    
    template<typename VT>
    VT VectorAdd(const VT& lhs, const VT& rhs) {
        VT ret = lhs;
        ret += rhs;
        return ret;
    }
    
    template<typename VT>
    VT& VectorAssign(VT& lhs, const VT& rhs) {
        lhs = rhs;
        return lhs;
    }
    
    template<typename VT>
    void VectorConstructor(void *memory)
    {
        // Initialize the pre-allocated memory by calling the
        // object constructor with the placement-new operator
        new(memory) VT();
    }
    
    template<typename VT>
    void VectorDestructor(void *memory)
    {
        // Uninitialize the memory by calling the object destructor
        ((VT*)memory)->~VT();
    }
    
    template<typename VT>
    void RegisterVectorType(asIScriptEngine* engine) {
        const char* typeName = VectorTypeName<VT>();
        engine->RegisterObjectType(typeName, sizeof(VT), asOBJ_VALUE | asOBJ_APP_CLASS_CDAK);
        
        engine->RegisterObjectMethod(typeName,
                                     (boost::format("%1% opAdd(%1%) const") % typeName).str().c_str(),
                                     asFUNCTIONPR(VectorAdd<VT>, (const VT&, const VT&), VT),
                                     asCALL_CDECL_OBJLAST);
        
        engine->RegisterObjectMethod(typeName,
                                     (boost::format("%1% opAssign(%1%) const") % typeName).str().c_str(),
                                     asFUNCTIONPR(VectorAssign<VT>, (VT&, const VT&), VT&),
                                     asCALL_CDECL_OBJLAST);
        
        engine->RegisterObjectBehaviour(typeName, asBEHAVE_CONSTRUCT, "void f()", asFUNCTION(VectorConstructor<VT>), asCALL_CDECL_OBJLAST);
        engine->RegisterObjectBehaviour(typeName, asBEHAVE_DESTRUCT, "void f()", asFUNCTION(VectorDestructor<VT>), asCALL_CDECL_OBJLAST);
        engine->RegisterObjectBehaviour(typeName, asBEHAVE_IMPLICIT_VALUE_CAST, "string f() const", asFUNCTION(VectorToString<VT>), asCALL_CDECL_OBJLAST);
    }
}

namespace Ark
{
    extern Ark::detail::ArkFormatter* formatter;
    
    Scripting* scripting_instance = nullptr;
    
    void script_message_callback(const asSMessageInfo *msg, void *param);
    
    
    
    void Scripting::initialize()
    {
        assert( scripting_instance == nullptr );
        
        scripting_instance = new Scripting();
    }
    void Scripting::teardown()
    {
        assert( scripting_instance != nullptr );
        
        delete scripting_instance;
    }
    
    Scripting* Scripting::instance()
    {
        assert( scripting_instance != nullptr );
        
        return scripting_instance;
    }
    
    
    
    Scripting::Scripting()
    {
        int r;
    
        LOGOG_INFO("Initializing AngelScript: version %s", ANGELSCRIPT_VERSION_STRING);
        _engine = asCreateScriptEngine(ANGELSCRIPT_VERSION);
        RegisterStdString(_engine);
        //RegisterScriptMath(_engine);
        //RegisterScriptMathComplex(_engine);
        
        r = _engine->SetMessageCallback(asFUNCTION(script_message_callback), 0, asCALL_CDECL);
        assert( r >= 0 );
        
        r = _engine->RegisterGlobalFunction("void print(const string &in)", asFUNCTION(print), asCALL_CDECL);
        assert(r >= 0);
        
        RegisterVectorType<glm::vec2>(_engine);
        RegisterVectorType<glm::i8vec2>(_engine);
        RegisterVectorType<glm::i32vec2>(_engine);
    }
    
    Scripting::~Scripting()
    {
        _engine->Release();
    }
    
    
    void script_message_callback(const asSMessageInfo *msg, void *param)
    {
        static std::string message_template = "SCRIPT: %s (%d, %d), %s";
        
        formatter->SetHideCPPDetails(true);
    
        if (msg->type == asMSGTYPE_ERROR)
        {
            LOGOG_ERROR(message_template.c_str(), msg->section, msg->row, msg->col, msg->message);
        }
        else if (msg->type == asMSGTYPE_WARNING)
        {
            LOGOG_WARN(message_template.c_str(), msg->section, msg->row, msg->col, msg->message);
        }
        else
        {
            LOGOG_INFO(message_template.c_str(), msg->section, msg->row, msg->col, msg->message);
        }
        
        formatter->SetHideCPPDetails(false);
    }
}