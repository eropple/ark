//
//  win32_io_redirect.cpp
//  Ark
//
//  Created by Ed Ropple on 2/12/13.
//  Copyright (c) 2013 Large Russian Games. All rights reserved.
//

#if WIN32

#include <Ark/win32_io_redirect.h>

#include <Windows.h>
#include <cstdio>
#include <fcntl.h>
#include <io.h>
#include <iostream>
#include <fstream>


namespace Ark
{
    namespace Win32
    {
        static const int MAX_CONSOLE_LINES = 3000;

        void redirect_io()
        {
            int hConHandle;
            long lStdHandle;

            CONSOLE_SCREEN_BUFFER_INFO coninfo;

            FILE *fp;
            AllocConsole();

            GetConsoleScreenBufferInfo(GetStdHandle(STD_OUTPUT_HANDLE), &coninfo);
            coninfo.dwSize.Y = MAX_CONSOLE_LINES;

            SetConsoleScreenBufferSize(GetStdHandle(STD_OUTPUT_HANDLE), coninfo.dwSize);

            lStdHandle = (long)GetStdHandle(STD_OUTPUT_HANDLE);
            hConHandle = _open_osfhandle(lStdHandle, _O_TEXT);
            fp = _fdopen(hConHandle, "w");

            *stdin = *fp;

            setvbuf(stdin, nullptr, _IONBF, 0);

            *stderr = *fp;

            setvbuf(stderr, nullptr, _IONBF, 0);

            std::ios::sync_with_stdio();
        }
    }
}

#endif