//
//  SimpleTexturedQuad.cpp
//  Ark
//
//  Created by Ed Ropple on 2/18/13.
//  Copyright (c) 2013 Large Russian Games. All rights reserved.
//

#include <Ark/SimpleTexturedQuad.h>

#include <Ark/GLTexture.h>
#include <Ark/Color4.h>
#include <Ark/ShaderProgram.h>

namespace Ark
{
    ShaderProgram* _simple_quad_default_shader;
    
    
    SimpleTexturedQuad::SimpleTexturedQuad(glm::ivec2 size, GLTexture* texture, Color4 color)
        :   RenderObject<SimpleTexturedVertexData>(build_definition(size), default_shader(), texture),
            _color(color)
    {
        
    }
    SimpleTexturedQuad::~SimpleTexturedQuad()
    {
        delete _definition; // unlike the default object, this one builds its own.
    }
    
    void SimpleTexturedQuad::draw(const glm::mat4& modelview, const glm::mat4& projection) const
    {
        glEnable(GL_BLEND);
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
        
        RenderObject::draw(modelview, projection);
    }
    
    RenderDefinition<SimpleTexturedVertexData>* SimpleTexturedQuad::build_definition(glm::ivec2 size)
    {
        std::vector<SimpleTexturedVertexData> vertices;
        vertices.push_back(SimpleTexturedVertexData { glm::vec3(0.0f, 0.0f, 0.0f), glm::vec2(0.0f, 0.0f) });
        vertices.push_back(SimpleTexturedVertexData { glm::vec3(0.0f, size.y, 0.0f), glm::vec2(0.0f, 1.0f) });
        vertices.push_back(SimpleTexturedVertexData { glm::vec3(size.x, 0.0f, 0.0f), glm::vec2(1.0f, 0.0f) });
        vertices.push_back(SimpleTexturedVertexData { glm::vec3(size.x, size.y, 0.0f), glm::vec2(1.0f, 1.0f) });
        
        std::vector<unsigned short> indices;
        indices.push_back(0); indices.push_back(1); indices.push_back(2); indices.push_back(3);
        
        // TODO: memoize this?
        std::vector<RenderDefinitionItem> attribs;
        RenderDefinitionItem vertex("vertex", 3, GL_FLOAT, GL_FALSE, 0);
        attribs.push_back(vertex);
        RenderDefinitionItem texcoords("texcoords", 2, GL_FLOAT, GL_FALSE, vertex.offset() + sizeof(glm::vec3));
        attribs.push_back(texcoords);
        
        return new RenderDefinition<SimpleTexturedVertexData>(vertices, attribs, indices);
    }
    
    
    ShaderProgram* SimpleTexturedQuad::default_shader()
    {
        if (_simple_quad_default_shader == nullptr)
        {
            _simple_quad_default_shader = ShaderProgram::from_yaml("/Game/Shaders/default_render_quad.yaml");
        }
        return _simple_quad_default_shader;
    }

}