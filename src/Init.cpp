//
//  InitArgs.cpp
//  Ark
//
//  Created by Ed Ropple on 1/24/13.
//  Copyright (c) 2013 Large Russian Games. All rights reserved.
//

#include <Ark/Init.h>
#include <Ark/Exception.h>
#include <Ark/Platform.h>
#include <Ark/ConfigManager.h>
#include <Ark/StateManager.h>
#include <Ark/Scripting.h>
#include <Ark/GLFW.h>

#include <boost/algorithm/string.hpp>

#include <logog/logog.hpp>

#include <physfs.h>

#include <list>

#if WIN32
    #include <cwchar>
#endif

namespace Ark
{
    namespace fs = boost::filesystem;
    
    logog::Cerr* logger;
    Ark::detail::ArkFormatter* formatter;
    
    void init_physfs(const InitArgs& args);
    void simple_mount(const fs::path& source_path, const std::string& mount_point, bool prepend);

    void initialize(const InitArgs& args)
    {
        LOGOG_INITIALIZE();
        
        logger = new logog::Cerr();
        
        formatter = new Ark::detail::ArkFormatter();
        logger->SetFormatter(*formatter);
        
        std::vector<fs::path> config_search_paths;
        config_search_paths.push_back(Platform::executable_path().parent_path());
        config_search_paths.push_back(Platform::user_data_root() / args.company_name() / args.application_name());
        
        ConfigManager::initialize(args.args(), config_search_paths);
        
        init_physfs(args);
        
        Scripting::initialize();
        
#if DESKTOP
        GLFW::initialize(args);
        auto glfw = GLFW::instance();
        args.global_content_heap()->_scale_factor = glfw->scale_factor();
        LOGOG_DEBUG("GLFW initialized. Scale factor: %f", args.global_content_heap()->scale_factor());
        
        auto window = GLFW::instance()->window_bounds();
        LOGOG_DEBUG("GLFW initialized. Window size: %d x %d", window.x, window.y);
#else
        #error Not yet implemented.
#endif

        StateManager::initialize(args.global_content_heap());
    }
    
    void teardown()
    {
        LOGOG_SHUTDOWN();
        delete logger;
        delete formatter;
    }
    
    void init_appdata_directory(const InitArgs& args)
    {
        try
        {
            fs::path p = Platform::user_data_root() / args.company_name() / args.application_name();
            if (!(fs::exists(p))) fs::create_directories(p);
        }
        catch (const boost::filesystem::filesystem_error& ex)
        {
            throw InitException("Failed to create app data directories: " + std::string(ex.what()));
        }
    }
    
    void init_physfs(const InitArgs& args)
    {
        if (PHYSFS_isInit())
        {
            LOGOG_WARN("PhysFS was already initialized; de-initing and hoping for the best.");
            PHYSFS_deinit();
        }
        PHYSFS_init(args.args()[0].c_str());
    
        std::vector<fs::path> mod_search_paths;
        
        auto game_mods_root = Platform::resource_root() / "Mods";
        auto user_mods_root = Platform::user_data_root() / args.company_name() / args.application_name() / "Mods";
        
        if (!fs::exists(user_mods_root) || !fs::is_directory(user_mods_root))
        {
            LOGOG_INFO("User mods root '%s' does not exist; skipping.", user_mods_root.c_str());
            mod_search_paths.push_back(user_mods_root);
        }
        if (!fs::exists(game_mods_root) || !fs::is_directory(game_mods_root))
        {
            throw InitException(game_mods_root.string() + " does not exist or is not a directory.");
        }
        mod_search_paths.push_back(game_mods_root);
        
        
        fs::path game_stuff_root = Platform::resource_root() / "Game";
        simple_mount(game_stuff_root, "/Game", true);
        
        auto mod_names = config()->mod_names();
        LOGOG_INFO("Mod names: %s", boost::algorithm::join(mod_names, " ").c_str());
        
        for (auto mod_name : mod_names)
        {
            fs::path mod_path;
            for (auto mod_search_path : mod_search_paths)
            {
                fs::path test_path = mod_search_path / mod_name;
                if (fs::is_directory(test_path))
                {
                    mod_path = test_path;
                    break; // for (mod_search_path ... )
                }
            }
            
            if (mod_path.empty())
            {
                throw InitException("No mod path found for mod named: " + mod_name);
            }
            
            simple_mount(mod_path, "/Mods/" + mod_name, false);
            
            fs::path override_path = mod_path / "Override";
            
            if (fs::is_directory(override_path))
            {
                simple_mount(override_path, "/Mods", false);
            }
        }
        
        fs::path writable_path(ConfigManager::instance()->get("writable.path",
                                                              Platform::user_data_root() /
                                                              args.company_name() /
                                                              args.application_name() / "SaveData"));
        
        try
        {
            if (!fs::exists(writable_path))
            {
                LOGOG_INFO("Writable path '%s' does not exist--attempting to create.", writable_path.c_str());
                fs::create_directories(writable_path);
            }
            PHYSFS_setWriteDir(writable_path.c_str());
        }
        catch ( const boost::filesystem::filesystem_error& ex )
        {
            throw InitException("Failure establishing writable path for PhysFS: " + std::string(ex.what()));
        }
    }
    
    
    
    void simple_mount(const fs::path& source_path, const std::string& mount_point, bool prepend)
    {
        LOGOG_INFO("Mounting '%s' to '%s'...", source_path.string().c_str(), mount_point.c_str());
#ifndef _MSC_VER
        PHYSFS_mount(source_path.c_str(), mount_point.c_str(), (prepend ? 0 : 1)); // prepends
#else
        const wchar_t* u16_path = source_path.c_str();
        size_t buf_length = wcslen(u16_path) * 2 + 1;
        char* buffer = new char[buf_length];
        
        PHYSFS_utf8FromUcs2((const PHYSFS_uint16*)u16_path, buffer, buf_length);
        
        PHYSFS_mount(buffer, mount_point.c_str(), (prepend ? 0 : 1)); // prepends;
        
        delete[] buffer;
#endif
    }
}