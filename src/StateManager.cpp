//
//  StateManager.cpp
//  Ark
//
//  Created by Ed Ropple on 12/26/12.
//  Copyright (c) 2012 Large Russian Games. All rights reserved.
//

#include <Ark/StateManager.h>
#include <Ark/Platform.h>

#include <logog/logog.hpp>

namespace Ark
{
    StateManager* state_instance = nullptr;
    
    void StateManager::initialize(ContentHeap* global_heap)
    {
        assert( state_instance == nullptr );
        state_instance = new StateManager(global_heap);
    }
    void StateManager::teardown()
    {
        assert( state_instance != nullptr );
        delete state_instance;
    }
    StateManager* StateManager::instance()
    {
        assert( state_instance != nullptr );
        return state_instance;
    }
    
    
    
    StateManager::StateManager(ContentHeap* global_heap)
            :   _global_heap(global_heap)
    {
        _states.reserve(50); // should never get that big.
        _deadStates.reserve(50); // ditto
    }
    
    StateManager::~StateManager()
    {
    }
    void StateManager::start_frame()
    {
//        LOGOG_DEBUG("# frames: %d", states.size());
    }
    
    
    
    void StateManager::key_handler(int key_identifier, bool pressed)
    {
        for (auto state : _states)
        {
            if (state->key_handler(key_identifier, pressed) || state->soaks_input()) return;
        }
    }
    void StateManager::char_handler(int unichar)
    {
        for (auto state : _states)
        {
            if (state->char_handler(unichar) || state->soaks_input()) return;
        }
    }
    void StateManager::mouse_position_handler(int x, int y, int dX, int dY)
    {
        for (auto state : _states)
        {
            if (state->mouse_position_handler(x, y, dX, dY) || state->soaks_input()) return;
        }
    }
    void StateManager::mouse_button_handler(int button, int x, int y, bool pressed)
    {
        for (auto state : _states)
        {
            if (state->mouse_button_handler(button, x, y, pressed) || state->soaks_input()) return;
        }
    }
    void StateManager::mouse_wheel_handler(double wheel_position, int wheel_direction, int x, int y)
    {
        for (auto state : _states)
        {
            if (state->mouse_wheel_handler(wheel_position, wheel_direction, x, y) || state->soaks_input()) return;
        }
    }
    
    void StateManager::update(double delta)
    {
        // if something pushes or pops from the stack, the state stack will be in
        // an unknown state, signified by setting this to true.
        _stack_is_dirty = false;
        
        for (auto iter = _states.rbegin(); iter != _states.rend(); ++iter)
        {
            auto state = *iter;
            state->do_update(delta, iter == _states.rbegin());
            if (_stack_is_dirty || state->soaks_updates()) break;
        }
    }
    
    void StateManager::draw(double delta, const glm::mat4& projection)
    {
        // We can't verify what state we're in for drawing if something has changed
        // the state stack. We'll draw again next frame.
        if (_stack_is_dirty || _states.size() < 1)
        {
            return;
        }
        
        auto reverse_iterator = _states.rbegin();
        for (; reverse_iterator != _states.rend(); ++reverse_iterator)
        {
            if ( (*reverse_iterator)->soaks_draws() ) break;
        }
        
        auto forward_iterator = reverse_iterator.base();
        forward_iterator = (forward_iterator == _states.end())
            ? _states.end() - 1
            : forward_iterator;
        
        for (; forward_iterator != _states.end(); ++forward_iterator)
        {
            (*forward_iterator)->do_draw(delta, projection, *forward_iterator == *(_states.rbegin()) );
            
            if (_stack_is_dirty) break;
        }
    }
    
    void StateManager::finish_frame()
    {
        for (auto state : _deadStates)
        {
            delete state;
        }
        _deadStates.clear();
    }
    void StateManager::push(State* newState)
    {
        _stack_is_dirty = true;
        
        State* oldState = (_states.size() > 0) ? _states[_states.size() - 1] : NULL;
        _states.push_back(newState);

        LOGOG_DEBUG("New state pushed (%s). Current state count: %d", typeid(*newState).name(), _states.size());
        
        if (oldState != NULL) oldState->lost_focus(newState);
        
        newState->do_initialize(_global_heap);
    }
    void StateManager::pop()
    {
        _stack_is_dirty = true;
        
        assert( _states.size() > 0 );
        
        State* oldState = _states[_states.size() - 1];
        _states.pop_back();
        State* newState = (_states.size() > 0) ? _states[_states.size() - 1] : NULL;
        
        LOGOG_DEBUG("Top state popped (%s). Current state count: %d", typeid(*oldState).name(), _states.size());
        
        if (newState != NULL) newState->got_focus(oldState);
    }
    void StateManager::pop_and_push(State* newState)
    {
        pop();
        push(newState);
    }
    
    bool StateManager::has_exited()
    {
        return _states.empty();
    }
}