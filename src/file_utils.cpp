//
//  file_utils.cpp
//  Ark
//
//  Created by Ed Ropple on 1/26/13.
//  Copyright (c) 2013 Large Russian Games. All rights reserved.
//

#include <Ark/file_utils.h>

#include <logog/logog.hpp>

#include <fstream>
#include <cerrno>

#include <yaml-cpp/yaml.h>
#include <physfs.h>

namespace Ark
{
    bool open_file(const std::string& key, PHYSFS_File** file)
    {
        const char* c_key = key.c_str();
        if (!PHYSFS_exists(c_key))
        {
            LOGOG_WARN("Could not find file '%s'.", c_key);
            return false;
        }
        
        *file = PHYSFS_openRead(c_key);
        return true;
    }

    bool read_string_from_file(PHYSFS_File* file, std::string& str)
    {
        int file_length = PHYSFS_fileLength(file);
        size_t array_length = file_length + 1;
        char* array = new char[array_length];
        PHYSFS_read (file, array, 1, file_length);
        array[array_length - 1] = '\0';
        
        str = std::string(array);
        
        delete[] array;
        
        return true;
    }
    
    bool read_vector_from_file(PHYSFS_File* file, std::vector<char>& vec)
    {
        int file_length = PHYSFS_fileLength(file);
        size_t array_length = file_length + 1;
        char* array = new char[array_length];
        PHYSFS_read (file, array, 1, file_length);
        
        vec = std::vector<char>(array, array + array_length);
        
        delete[] array;
        
        return true;
    }
    
    bool read_yaml_from_file(PHYSFS_File* file, YAML::Node& yaml)
    {
        try
        {
            std::string text;
            
            read_string_from_file(file, text);
            yaml = YAML::Load(text);
            
            return true;
        }
        catch (...)
        {
            return false;
        }
    }
    
    
    
    bool read_xml_from_file(PHYSFS_File* file, rapidxml::xml_document<>& xml)
    {
        try
        {
            std::vector<char> text_vec;
            
            read_vector_from_file(file, text_vec);
            xml.parse<rapidxml::parse_no_data_nodes>(&text_vec[0]);
            
            return true;
        }
        catch (...)
        {
            return false;
        }
    }
    
    
    bool list_entities(const std::string& directory_path, std::vector<std::string>& entities)
    {
        if (!PHYSFS_isDirectory(directory_path.c_str())) return false;
        
        entities.clear();
        
        char** rc = PHYSFS_enumerateFiles(directory_path.c_str());
        char** i;
        
        for (i = rc; *i != nullptr; ++i)
        {
            entities.push_back(*i);
        }
        
        PHYSFS_freeList(rc);
        
        return true;
    }
}
