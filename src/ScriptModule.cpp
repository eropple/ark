//
//  ScriptModule.cpp
//  Ark
//
//  Created by Ed Ropple on 1/17/13.
//  Copyright (c) 2013 Large Russian Games. All rights reserved.
//

#include <Ark/Exception.h>
#include <Ark/file_utils.h>
#include <Ark/ScriptModule.h>
#include <Ark/Scripting.h>
#include <AngelScript/scriptbuilder.h>

#include <boost/lexical_cast.hpp>
#include <boost/uuid/uuid.hpp>
#include <boost/uuid/uuid_generators.hpp>
#include <boost/uuid/uuid_io.hpp>

#include <logog/logog.hpp>

namespace Ark
{
    namespace fs = boost::filesystem;
    
    boost::uuids::random_generator uuid_generator;

    ScriptModule::ScriptModule(const boost::unordered_map<std::string, PHYSFS_File*>& files)
    {
        assert( files.size() > 0 );
    
        _module = nullptr;
        
        CScriptBuilder builder;
        _name = boost::lexical_cast<std::string>(uuid_generator());
        const char* n = _name.c_str();
        
        LOGOG_INFO("Building ScriptModule with UUID '%s'.", n);
        
        int r = -1;
        
        r = builder.StartNewModule(Scripting::instance()->engine(), n);
        if (r < 0)
        {
            LOGOG_ERROR("Initialization of the builder for script '%s' failed hard (code %d).", n, r);
        }
        else
        {
            for (auto iter = files.begin(); iter != files.end(); ++iter)
            {
                std::string str;
                read_string_from_file(iter->second, str);
            
                r = builder.AddSectionFromMemory(str.c_str());
                if (r < 0)
                {
                    throw ContentException("Module '" + _name + "': Failed during compilation of '" +
                                           iter->first + "' (code " + boost::lexical_cast<std::string>(r) + "");
                }
            }
            
            if (r >= 0)
            {
                r = builder.BuildModule();
                if (r < 0)
                {
                    throw ContentException("Module '" + _name + "': Failed to build module (code " +
                                           boost::lexical_cast<std::string>(r) + ")");
                }
                else
                {
                    _module = Scripting::instance()->engine()->GetModule(n);
                }
            }
        }
        
    }
    ScriptModule::~ScriptModule()
    {
        Scripting::instance()->engine()->DiscardModule(_name.c_str());
    }
}