//
//  Color4.cpp
//  Ark
//
//  Created by Ed Ropple on 1/23/13.
//  Copyright (c) 2013 Large Russian Games. All rights reserved.
//

#include "Ark/Color4.h"

namespace Ark
{
    Color4::Color4(unsigned char red, unsigned char green, unsigned char blue, unsigned char alpha)
    {
        this->r = red == 0 ? 0.0f : red / 255.0f;
        this->g = green == 0 ? 0.0f : green / 255.0f;
        this->b = blue == 0 ? 0.0f : blue / 255.0f;
        this->a = alpha == 0 ? 0.0f : alpha / 255.0f;
    }
}