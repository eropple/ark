//
//  platform.cpp
//  Ark
//
//  Created by Ed Ropple on 12/26/12.
//  Copyright (c) 2012 Large Russian Games. All rights reserved.
//

#include <Ark/ConfigManager.h>
#include <Ark/Platform.h>

#if WIN32
    #include <ShlObj.h>
#endif
#if MACOS
    #include <dlfcn.h>
    #include <CoreFoundation/CoreFoundation.h>
#endif

namespace Ark
{
    namespace fs = boost::filesystem;
    
    namespace Platform
    {   
        fs::path executable_path()
        {
    #if WIN32
            wchar_t path_buffer[2048];
            
            GetModuleFileNameW(NULL, path_buffer, 2048);
            
            return fs::path(std::wstring(path_buffer));
    #elif MACOS
            // Code from http://jeffcode.blogspot.com/2010/03/getmodulefilename-on-os-x.html
            Dl_info module_info;
            if (dladdr(reinterpret_cast<void*>(executable_path), &module_info) == 0)
            {
                // Failed to find the symbol we asked for.
                return std::string();
            }
            return std::string(module_info.dli_fname);
    #else
            #error TODO: implement get_executable_path
    #endif
        }
        
        fs::path resource_root()
        {
    #if WIN32 || LINUX
            return executable_path().parent_path();
    #elif MACOS
            CFBundleRef mainBundle = CFBundleGetMainBundle();
            if (mainBundle == NULL)
            {
                return executable_path().parent_path();
            }
            else
            {
                CFURLRef resources = CFBundleCopyResourcesDirectoryURL(mainBundle);
                unsigned char buffer[2048];
                bool retval = CFURLGetFileSystemRepresentation(resources, true, buffer, 2048);
                
                if (retval)
                {
                    return fs::path(reinterpret_cast<char*>(buffer));
                }
                else
                {
                    return executable_path().parent_path();
                }
            }
    #else
            #error TODO: define get_resource_root() for this platform.
    #endif
        }
        
        
        
        
        fs::path user_data_root()
        {
    #if WIN32
            wchar_t buffer[2048];
            SHGetFolderPathW(NULL, CSIDL_LOCAL_APPDATA|CSIDL_FLAG_CREATE, NULL, 0, buffer);
            
            return fs::path(buffer);;
    #elif MACOS
            // TODO: this is super lame. fix this for sandbox mode.
            return fs::path("~/Library/Application Support");
    #else
            #error TODO: platform implementation for get_user_data_root()
    #endif
        }
    }
}