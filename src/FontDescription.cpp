//
//  FontDescription.cpp
//  Ark
//
//  Created by Ed Ropple on 2/16/13.
//  Copyright (c) 2013 Large Russian Games. All rights reserved.
//

#include <Ark/FontDescription.h>
#include <Ark/file_utils.h>

#include <rapidxml/rapidxml.hpp>
#include <boost/lexical_cast.hpp>

namespace Ark
{
    FontDescription::FontDescription(const std::string& key)
    {
        using namespace rapidxml;
        
        PHYSFS_file* file;
        open_file(key, &file);
        
        xml_document<> xml;
        read_xml_from_file(file, xml);
        
        xml_node<>* root = xml.first_node("font");
        
        xml_node<>* info = root->first_node("info");
        xml_node<>* common = root->first_node("common");
        
        _size = boost::lexical_cast<int>(info->first_attribute("size")->value());
        _line_height = boost::lexical_cast<int>(common->first_attribute("lineHeight")->value());
        _baseline = boost::lexical_cast<int>(common->first_attribute("base")->value());
        
        boost::unordered_map<std::pair<int, int>, float> kernings;
        
        xml_node<>* pages_node = root->first_node("pages");
        for (xml_node<> *child = pages_node->first_node(); child; child = child->next_sibling())
        {
            _pages.insert(std::make_pair(boost::lexical_cast<int>(child->first_attribute("id")->value()),
                                        std::string(child->first_attribute("file")->value())));
        }
        
        xml_node<>* chars_node = root->first_node("chars");
        for (xml_node<> *child = chars_node->first_node(); child; child = child->next_sibling())
        {
            std::string letter_text(child->first_attribute("letter")->value());
            char letter = (letter_text == "space") ? ' ' : letter_text[0];
            int x = boost::lexical_cast<int>(child->first_attribute("x")->value());
            int y = boost::lexical_cast<int>(child->first_attribute("y")->value());
            int w = boost::lexical_cast<int>(child->first_attribute("width")->value());
            int h = boost::lexical_cast<int>(child->first_attribute("height")->value());
            
            int xo = boost::lexical_cast<int>(child->first_attribute("xoffset")->value());
            int yo = boost::lexical_cast<int>(child->first_attribute("yoffset")->value());
            
            auto xadv_attr = child->first_attribute("xadvance");
            int xadv = boost::lexical_cast<int>(xadv_attr->value());
            
            int page = boost::lexical_cast<int>(child->first_attribute("page")->value());
            int channel = boost::lexical_cast<int>(child->first_attribute("chnl")->value());
            
//            FontCharacter(int letter, int x, int y, int width, int height,
//                          int xoffset, int yoffset, int xadvance,
//                          int page, int channel)
            
            auto cc = new FontCharacter(letter, x, y, w, h, xo, yo, xadv, page, channel);
            _characters.insert(std::make_pair(letter, cc));
        }
        
        xml_node<>* kernings_node = root->first_node("kernings");
        if (kernings_node != nullptr)
        {
            for (xml_node<> *child = kernings_node->first_node(); child; child = child->next_sibling())
            {
                int first = boost::lexical_cast<int>(child->first_attribute("first")->value());
                int second = boost::lexical_cast<int>(child->first_attribute("second")->value());
                int amount = boost::lexical_cast<int>(child->first_attribute("amount")->value());
                
                _kernings.insert(std::make_pair(std::make_pair(first, second), amount));
            }
        }

    }
    FontDescription::~FontDescription()
    {
        for (auto pair : _characters) delete pair.second;
    }
    
    const FontCharacter* FontDescription::character(int char_value) const
    {
        auto iter = _characters.find(char_value);
        return (iter == _characters.end()) ? nullptr : iter->second;
    }
    
    float FontDescription::kerning(int char1, int char2) const
    {
        auto pair = std::make_pair(char1, char2);
        auto iter = _kernings.find(pair);
        return (iter == _kernings.end()) ? 0 : iter->second;
    }
}