//
//  Font.cpp
//  Ark
//
//  Created by Ed Ropple on 2/18/13.
//  Copyright (c) 2013 Large Russian Games. All rights reserved.
//

#include <Ark/Font.h>
#include <Ark/FontDescription.h>
#include <Ark/Exception.h>
#include <Ark/MemoryBitmap.h>
#include <Ark/GLTexture.h>
#include <Ark/TextDrawables.h>
#include <Ark/file_utils.h>

#include <boost/lexical_cast.hpp>
#include <boost/regex.hpp>
#include <boost/filesystem.hpp>
#include <boost/algorithm/string.hpp>

#include <algorithm>
#include <cstdio>

namespace Ark
{
    const boost::regex COLOR_CODE_REGEX("\\[\#([Xx]|[0-9a-fA-F]{6})\\]");
    const int COLOR_RESET_LENGTH = 4;
    const int COLOR_CODE_LENGTH = 9;
    
    namespace fs = boost::filesystem;

    void load_glyphs(const fs::path& page_root, FontDescription* desc, boost::unordered_map<int, GLTexture*>& map)
    {
        // BMFont spec does not guarantee a non-sparse list of page indices.
        boost::unordered_map<int, MemoryBitmap*> bitmaps;
        
        for (auto page_path : desc->pages())
        {
            std::string tex_key = (page_root / page_path.second).string();
            LOGOG_DEBUG("Loading font page %d: %s", page_path.first, tex_key.c_str());
            PHYSFS_file* file;
            open_file(tex_key, &file);
            bitmaps.insert(std::make_pair(page_path.first, new MemoryBitmap(file)));
        }
        
        for (auto pair : desc->characters())
        {
            auto character = pair.second;
            auto iter = bitmaps.find(character->page());
            
            if (iter == bitmaps.end())
            {
                throw ContentException("Content page '" + boost::lexical_cast<std::string>(character->page()) + "' not found.");
            }
            
            auto bitmap = iter->second;
            
            auto rect = character->rect();
            auto slice = bitmap->copy_slice(rect.x, rect.y, rect.width, rect.height);
            
            auto texture = new GLTexture(slice);
            map.insert(std::make_pair(character->letter(), texture));
            
            delete slice;
        }
        
        for (auto pair : bitmaps)
        {
            delete pair.second;
        }
    }
    
    void create_quads(FontDescription* desc, const boost::unordered_map<int, GLTexture*>& textures,
                      boost::unordered_map<int, SimpleTexturedQuad*>& map)
    {
        for (auto pair : desc->characters())
        {
            auto character = pair.second;
            
            auto iter = textures.find(character->letter());
            if (iter == textures.end())
            {
                throw ContentException("No GLTexture found for char: " + boost::lexical_cast<std::string>(character->letter()));
            }
            
            auto texture = iter->second;
            
            auto rect = character->rect();
            
            if (character->letter() == 'A')
            {
                LOGOG_DEBUG("'A' size: %d x %d", rect.width, rect.height);
            }
            
            auto quad = new SimpleTexturedQuad(glm::ivec2(rect.width, rect.height), texture);
            map.insert(std::make_pair(character->letter(), quad));
        }
    }

    Font::Font(const std::string& hidpi_key, bool use_hidpi)
    {
        LOGOG_DEBUG("Loading font '%s', hidpi = %d", hidpi_key.c_str(), use_hidpi);
    
        std::string standard_key = hidpi_key;
        boost::replace_all(standard_key, ".fnt", "-sd.fnt");
        
        _standard_font = new FontDescription(standard_key);
        _hidpi_font = use_hidpi ? new FontDescription(hidpi_key) : nullptr;
        
        auto glyph_src = use_hidpi ? _hidpi_font : _standard_font;
        load_glyphs(fs::path(hidpi_key).parent_path(), glyph_src, _textures);
        create_quads(_standard_font, _textures, _quads);
    }
    
    Font::~Font()
    {
        for(auto pair : _textures) delete pair.second;
        for(auto pair : _quads) delete pair.second;
    
        delete _standard_font;
        if (_hidpi_font != nullptr) delete _hidpi_font;
    }
    
    const GLTexture* Font::texture(int character) const
    {
        auto iter = _textures.find(character);
        if (iter == _textures.end()) return nullptr;
        
        return iter->second;
    }
    SimpleTexturedQuad* Font::quad(int character) const
    {
        auto iter = _quads.find(character);
        if (iter == _quads.end()) return nullptr;
        
        return iter->second;
    }
    
    
    float Font::measure_string_height() const
    {
        return _standard_font->line_height();
    }
    
    float Font::measure_string_width(const std::string& text, bool strip_codes) const
    {
        std::string measure_text = strip_codes ? strip_color_codes(text) : text;
        if (measure_text.empty()) return 0;
        if (measure_text.size() == 1)
        {
            return texture(measure_text[0])->bounds().x;
        }
        
        float x = 0;
        
        for (int i = 0; i < (measure_text.size() - 1); ++i)
        {
            int c0 = measure_text[i];
            int c1 = measure_text[i + 1];
            
            x += _standard_font->character(c0)->xadvance();
            x += _standard_font->kerning(c0, c1);
        }
        
        return x;
    }
    
    TextDrawable* Font::build_text_drawable(const std::string& text,
                                            const Color4& color,
                                            int wrap_length)
    {
        Color4 current_color = color;
        
        std::stringstream word;
        std::stringstream line;
        
        std::vector<LineDrawable*> lines;
        std::vector<GlyphDrawable*> line_glyphs;
        std::vector<GlyphDrawable*> word_glyphs;
        
        float word_length = 0;
        float line_length = 0;
        
        float line_y = 0;
        
        int i = 0;
        while (i < text.length())
        {
            char c = text[i];
            
            if (i < (text.length() - COLOR_RESET_LENGTH) && c == '[' && text[i + 1] == '#') // control characters
            {
                auto c2 = text[i + 2];
                auto c3 = text[i + 3];
                if ( (c2 == 'x' || c2 == 'X') && c3 == ']') // we found [#x]
                {
#if DEBUG
                    LOGOG_DEBUG("font: reset found, resetting to font normal color.");
#endif
                    current_color = color;
                    i += COLOR_RESET_LENGTH;
                }
                else if (i < text.length() - COLOR_CODE_LENGTH)
                {
                    std::string color_str = text.substr(i + 2, COLOR_CODE_LENGTH - 3);
//                    if (!boost::regex_match(color_str, COLOR_CODE_REGEX))
//                    {
//                        throw FontException("Invalid color code sequence: '" + color_str + "'");
//                    }
                    
                    std::string rs = color_str.substr(0, 2);
                    std::string gs = color_str.substr(2, 2);
                    std::string bs = color_str.substr(4, 2);
                    int r;
                    int g;
                    int b;
                    
                    sscanf(rs.c_str(), "%x", &r);
                    sscanf(gs.c_str(), "%x", &g);
                    sscanf(bs.c_str(), "%x", &b);
                    
#if DEBUG
                    LOGOG_DEBUG("font: color code found, RGB(%d, %d, %d).", r, g, b);
#endif
                    current_color = Color4((unsigned char)r, (unsigned char)g, (unsigned char)b, 255);
                    i += COLOR_CODE_LENGTH;
                }
            }
            else // any non-control character
            {
            
                const FontCharacter* char_info = _standard_font->character((int)c);
                word << c;
                word_glyphs.push_back(new GlyphDrawable(char_info, current_color, quad(c)));
                word_length += char_info->xadvance();
                i += 1;
            
                if (c == ' ') // end of word
                {
                    if (wrap_length != -1) // if word wrapping is on
                    {
                        if (word_length + line_length > wrap_length)
                        {
                            LineDrawable* line_drawable = new LineDrawable(glm::vec3(0, line_y, 0), _standard_font, line_glyphs); // finish the line
                            lines.push_back(line_drawable);
                            line_y += measure_string_height();
                            
                            line_glyphs.clear(); // clears the line, copies the word to the new line
                            for (auto glyph : word_glyphs) line_glyphs.push_back(glyph);
                            line.str(word.str());
                            line_length = word_length;
                            word_glyphs.clear();
                            word.str(std::string());
                            word_length = 0.0f;
                        }
                    }
                    else // no word wrapping, just add to list and clear the word.
                    {
                        for (auto glyph : word_glyphs) line_glyphs.push_back(glyph);
                        line << word.str();
                        line_length += word_length;
                        word_glyphs.clear();
                        word.str(std::string());
                        word_length = 0.0f;
                    }
                }
            }
        } // end of while loop
        
        if (wrap_length != -1) // if word wrapping is on
        {
            if (word_length + line_length > wrap_length)
            {
                LineDrawable* line_drawable = new LineDrawable(glm::vec3(0, line_y, 0), _standard_font, line_glyphs); // finish the line
                lines.push_back(line_drawable);
                line_y += measure_string_height();
                
                line_glyphs.clear(); // clears the line, copies the word to the new line
                for (auto glyph : word_glyphs) line_glyphs.push_back(glyph);
                line.str(word.str());
                line_length = word_length;
                word_glyphs.clear();
                word.str(std::string());
                word_length = 0.0f;
            }
        }
        else // no word wrapping, just add to list and clear the word.
        {
            for (auto glyph : word_glyphs) line_glyphs.push_back(glyph);
            line << word.str();
            line_length += word_length;
            word_glyphs.clear();
            word.str(std::string());
            word_length = 0.0f;
        }
        
        LineDrawable* line_drawable = new LineDrawable(glm::vec3(0, line_y, 0), _standard_font, line_glyphs); // finish the line
        lines.push_back(line_drawable);
        
        return new TextDrawable(lines);
    }
    
    
    
    
    std::string Font::strip_color_codes(const std::string& text)
    {
        // all color codes are of the format '[#RRGGBB]'.
        return boost::regex_replace(text, COLOR_CODE_REGEX, "");
    }
}