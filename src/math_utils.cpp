//
//  math_utils.cpp
//  Ark
//
//  Created by Ed Ropple on 1/20/13.
//  Copyright (c) 2013 Large Russian Games. All rights reserved.
//

#include "Ark/math_utils.h"
#include "Ark/Rect.h"
#include <cmath>

namespace Ark
{
    FloatRect compute_best_fit_rectangle(const FloatRect& rect, const FloatRect& container, bool round)
    {
        
        float scaled_height = container.height;
        float scaled_width = rect.width * container.height / rect.height;
        
        if (scaled_width > container.width)
        {
            scaled_width = container.width;
            scaled_height = rect.height * container.width / rect.width;
        }
        
        return (!round) ? FloatRect(0, 0, scaled_width, scaled_height)
                        : FloatRect(0, 0, roundf(scaled_width), roundf(scaled_height));
    }
}