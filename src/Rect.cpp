//
//  Rect.cpp
//  Ark
//
//  Created by Ed Ropple on 1/23/13.
//  Copyright (c) 2013 Large Russian Games. All rights reserved.
//

#include "Ark/Rect.h"

namespace Ark
{
    std::vector<glm::vec2> FloatRect::vertices() const
    {
        std::vector<glm::vec2> vecs;
        
        vecs.push_back(glm::vec2(x, y));
        vecs.push_back(glm::vec2(x + width, y));
        vecs.push_back(glm::vec2(x + width, y + height));
        vecs.push_back(glm::vec2(x, y + height));
        
        return vecs;
    }
    
    std::vector<glm::ivec2> IntRect::vertices() const
    {
        std::vector<glm::ivec2> vecs;
        
        vecs.push_back(glm::ivec2(x, y));
        vecs.push_back(glm::ivec2(x + width, y));
        vecs.push_back(glm::ivec2(x + width, y + height));
        vecs.push_back(glm::ivec2(x, y + height));
        
        return vecs;
    }
}